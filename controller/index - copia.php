<?php

/*
 * Controllador para realizar la Conexión
 */

class IndexController
{

    private $service;
    private $nameDB;
    private $db;
    private $countries;
    private $country;

    public function __construct($nameDB, $country)
    {
        $_GET['message'] = '';
        $this->nameDB = $nameDB;
        $this->country = $country;
        $this->service = array();
        $mysqli = mysqli_connect("localhost", "root", "", $nameDB);
        //$mysqli = mysqli_connect("172.102.100.121", "linde_test", "Linde2018*", $nameDB);

        if (!$mysqli) {
            $message = "No se pudo conectar a MySQL. Error: ".mysqli_connect_errno();
            header('Location: index.php?error='.$message);
            exit;
        }
        $this->db = $mysqli;
    }

    public function depuration()
    {
        if (strcmp($this->country, 'latam') === 0) {
            $like = 'NOT LIKE';
        } else {
            $like = 'LIKE';
        }
        $like_full = " p.name ". $like ." 'colombia' ";
        $this->countries = $this->db->query("SELECT * FROM allus_le_paises WHERE name " . $like . " 'colombia'");

        if ($this->countries->num_rows > 0) {
            $file = fopen("insert.txt", "w");
            fwrite($file, '');
            fclose($file);

            while ($row = $this->countries->fetch_assoc()) {
                $countries[] = $row;
            }

            foreach ($countries as $key => $object) {
                $paises_id[] = $object['id'];
            }

            //Depuración de allus_le_paises_audit
            if (empty($paises_id) == false) {
                $paises_id = implode("', '", $paises_id);
                $this->deleteAuditGeneral($paises_id, 'allus_le_paises');
            }

            //Depuración de users
            $user_full_row = $this->db->query("SELECT * FROM users WHERE user_pais_id IS NULL");
            $user_full = [];

            if ($user_full_row->num_rows > 0) {
                while ($row = $user_full_row->fetch_assoc()) {
                    $user_full[] = $row;
                }

                foreach ($user_full as $key => $object) {
                    $this->createInsertUsers($object, 'users');
                }
            }

            //Depuración de users
            $user_row = $this->db->query("SELECT * FROM users u INNER JOIN allus_le_paises p ON p.id = u.user_pais_id WHERE ".$like_full." AND u.user_name != 'admin.allus'");
            $users = [];

            if ($user_row->num_rows > 0) {
                while ($row = $user_row->fetch_assoc()) {
                    $users[] = $row;
                }

                foreach ($users as $key => $object) {
                    $this->createInsert($object, 'users');
                }
            }

            //Depuración de allus_le_paises_users_rel asociadas a los Países
            $allus_le_paises_users_rel_row = $this->db->query("SELECT * FROM allus_le_paises_users_rel WHERE allus_le_paises_id IN ('" . $paises_id . "')");
            $allus_le_paises_users_rel = [];

            if ($allus_le_paises_users_rel_row->num_rows > 0) {
                while ($row = $allus_le_paises_users_rel_row->fetch_assoc()) {
                    $allus_le_paises_users_rel[] = $row;
                }

                foreach ($allus_le_paises_users_rel as $key => $object) {
                    $this->createInsert($object, 'allus_le_paises_users_rel');
                }
            }

            die('as');

            if(empty($paises_id) == false){
                //Depuración de allus_em_accounts por País eliminados
                $allus_em_accounts_row = $this->db->query("SELECT * FROM allus_em_accounts WHERE pais_id IN ('" . $paises_id . "')");
                $allus_em_accounts = [];
                $allus_em_accounts_id = [];

                if ($allus_em_accounts_row->num_rows > 0) {
                    while ($row = $allus_em_accounts_row->fetch_assoc()) {
                        $allus_em_accounts[] = $row;
                    }

                    foreach ($allus_em_accounts as $key => $object) {
                        $allus_em_accounts_id[] = $object['id'];
                        $this->createInsert($object, 'accounts');
                    }
                }

                if (empty($allus_em_accounts_id) == false) {
                    $allus_em_accounts_id = implode("', '", $allus_em_accounts_id);
                    //Depuración de allus_em_emails asociadas a las cuentas eliminadas
                    $emails_row = $this->db->query("SELECT * FROM allus_em_emails WHERE em_accounts_id IN ('" . $allus_em_accounts_id . "')");
                    $emails = [];
                    $emails_id = [];

                    if ($emails_row->num_rows > 0) {
                        while ($row = $emails_row->fetch_assoc()) {
                            $emails[] = $row;
                        }

                        foreach ($emails as $key => $object) {
                            $emails_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_emails');
                        }
                    }

                    if(empty($emails_id) == false){
                        $emails_id = implode("', '", $emails_id);
                        //Depuración de allus_em_emails_cases_rel asociadas a las cuentas eliminadas
                        $allus_em_emails_cases_rel_row = $this->db->query("SELECT * FROM allus_em_emails_cases_rel WHERE em_emails_id IN ('" . $emails_id . "')");
                        $allus_em_emails_cases_rel = [];
                        $allus_em_emails_cases_rel_id = [];

                        if ($allus_em_emails_cases_rel_row->num_rows > 0) {
                            while ($row = $allus_em_emails_cases_rel_row->fetch_assoc()) {
                                $allus_em_emails_cases_rel[] = $row;
                            }

                            foreach ($allus_em_emails_cases_rel as $key => $object) {
                                $allus_em_emails_cases_rel_id[] = $object['id'];
                                $this->createInsert($object, 'allus_em_emails_cases_rel');
                            }
                        }

                        $allus_em_emails_grupos_rel_row = $this->db->query("SELECT * FROM allus_em_emails_grupos_rel WHERE email_id IN ('" . $emails_id . "')");
                        $allus_em_emails_grupos_rel = [];
                        $allus_em_emails_grupos_rel_id = [];

                        if ($allus_em_emails_grupos_rel_row->num_rows > 0) {
                            while ($row = $allus_em_emails_grupos_rel_row->fetch_assoc()) {
                                $allus_em_emails_grupos_rel[] = $row;
                            }

                            foreach ($allus_em_emails_grupos_rel as $key => $object) {
                                $allus_em_emails_grupos_rel_id[] = $object['id'];
                                $this->createInsert($object, 'allus_em_emails_grupos_rel');
                            }
                        }

                        $allus_em_emails_logs_row = $this->db->query("SELECT * FROM allus_em_emails_logs WHERE em_emails_id IN ('" . $emails_id . "')");
                        $allus_em_emails_logs = [];
                        $allus_em_emails_logs_id = [];

                        if ($allus_em_emails_logs_row->num_rows > 0) {
                            while ($row = $allus_em_emails_logs_row->fetch_assoc()) {
                                $allus_em_emails_logs[] = $row;
                            }

                            foreach ($allus_em_emails_logs as $key => $object) {
                                $allus_em_emails_logs_id[] = $object['id'];
                                $this->createInsert($object, 'allus_em_emails_logs');
                            }
                        }

                        $allus_em_emails_to_allus_em_labels_xref_row = $this->db->query("SELECT * FROM allus_em_emails_to_allus_em_labels_xref WHERE allus_em_emails_id IN ('" . $emails_id . "')");
                        $allus_em_emails_to_allus_em_labels_xref = [];
                        $allus_em_emails_to_allus_em_labels_xref_id = [];

                        if ($allus_em_emails_to_allus_em_labels_xref_row->num_rows > 0) {
                            while ($row = $allus_em_emails_to_allus_em_labels_xref_row->fetch_assoc()) {
                                $allus_em_emails_to_allus_em_labels_xref[] = $row;
                            }

                            foreach ($allus_em_emails_to_allus_em_labels_xref as $key => $object) {
                                $allus_em_emails_to_allus_em_labels_xref_id[] = $object['id'];
                                $this->createInsert($object, 'allus_em_emails_to_allus_em_labels_xref');
                            }
                        }
                    }

                    //SDepuración de allus_em_labels asociadas a las cuentas eliminadas
                    $allus_em_labels_row = $this->db->query("SELECT * FROM allus_em_labels WHERE em_accounts_id IN ('" . $allus_em_accounts_id . "')");
                    $allus_em_labels = [];
                    $allus_em_labels_id = [];

                    if ($allus_em_labels_row->num_rows > 0) {
                        while ($row = $allus_em_labels_row->fetch_assoc()) {
                            $allus_em_labels[] = $row;
                        }

                        foreach ($allus_em_labels as $key => $object) {
                            $allus_em_labels_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_labels');
                        }
                    }

                    //Depuración de allus_em_mailboxes asociadas a las cuentas eliminadas
                    $allus_em_mailboxes_row = $this->db->query("SELECT * FROM allus_em_mailboxes WHERE em_accounts_id IN ('" . $allus_em_accounts_id . "')");
                    $allus_em_mailboxes = [];
                    $allus_em_mailboxes_id = [];

                    if ($allus_em_mailboxes_row->num_rows > 0) {
                        while ($row = $allus_em_mailboxes_row->fetch_assoc()) {
                            $allus_em_mailboxes[] = $row;
                        }

                        foreach ($allus_em_mailboxes as $key => $object) {
                            $allus_em_mailboxes_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_mailboxes');
                        }
                    }

                    //Depuración de allus_em_accounts_to_allus_grupos_xref asociadas a las cuentas eliminadas
                    $allus_em_accounts_to_allus_grupos_xref_row = $this->db->query("SELECT * FROM allus_em_accounts_to_allus_grupos_xref WHERE allus_em_accounts_id IN ('" . $allus_em_accounts_id . "')");
                    $allus_em_accounts_to_allus_grupos_xref = [];
                    $allus_em_accounts_to_allus_grupos_xref_id = [];

                    if ($allus_em_accounts_to_allus_grupos_xref_row->num_rows > 0) {
                        while ($row = $allus_em_accounts_to_allus_grupos_xref_row->fetch_assoc()) {
                            $allus_em_accounts_to_allus_grupos_xref[] = $row;
                        }

                        foreach ($allus_em_accounts_to_allus_grupos_xref as $key => $object) {
                            $allus_em_accounts_to_allus_grupos_xref_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_accounts_to_allus_grupos_xref');
                        }
                    }
                }

                //Depuración de allus_clientes relacionas con Paises
                $allus_business_days_row = $this->db->query("SELECT * FROM allus_business_days WHERE country_id IN ('" . $paises_id . "')");
                $allus_business_days = [];
                $allus_business_days_id = [];

                if ($allus_business_days_row->num_rows > 0) {
                    while ($row = $allus_business_days_row->fetch_assoc()) {
                        $allus_business_days[] = $row;
                    }

                    foreach ($allus_business_days as $key => $object) {
                        $allus_business_days_id[] = $object['id'];
                        $this->createInsert($object, 'allus_business_days');
                    }
                    $allus_business_days_id = implode("', '", $allus_business_days_id);
                }

                //Depuración de allus_clientes relacionas con Paises
                $allus_clientes_row = $this->db->query("SELECT * FROM allus_clientes WHERE clie_pais_id IN ('" . $paises_id . "')");
                $allus_clientes = [];
                $allus_clientes_id = [];

                if ($allus_clientes_row->num_rows > 0) {
                    while ($row = $allus_clientes_row->fetch_assoc()) {
                        $allus_clientes[] = $row;
                    }

                    foreach ($allus_clientes as $key => $object) {
                        $allus_clientes_id[] = $object['id'];
                        $this->createInsert($object, 'allus_clientes');
                    }
                    $allus_clientes_id = implode("', '", $allus_clientes_id);
                    $this->deleteAuditGeneral($allus_clientes_id, 'allus_clientes');
                }

                if(empty($allus_clientes_id) == false){
                    //Depuración de allus_contactos relacionados con Clientes
                    $allus_contactos_row = $this->db->query("SELECT * FROM allus_contactos WHERE cont_clie_id IN ('" . $allus_clientes_id . "')");
                    $allus_contactos = [];
                    $allus_contactos_id = [];

                    if ($allus_contactos_row->num_rows > 0) {
                        while ($row = $allus_contactos_row->fetch_assoc()) {
                            $allus_contactos[] = $row;
                        }

                        foreach ($allus_contactos as $key => $object) {
                            $allus_contactos_id[] = $object['id'];
                            $this->createInsert($object, 'allus_contactos');
                        }
                        $allus_contactos_id = implode("', '", $allus_contactos_id);
                        $this->deleteAuditGeneral($allus_contactos_id, 'allus_contactos');
                    }

                    //Depuración de allus_em_emails_allus_clientes_rel relacionados con Clientes
                    $allus_em_emails_allus_clientes_rel_row = $this->db->query("SELECT * FROM allus_em_emails_allus_clientes_rel WHERE allus_cliente_id IN ('" . $allus_clientes_id . "')");
                    $allus_em_emails_allus_clientes_rel = [];
                    $allus_em_emails_allus_clientes_rel_id = [];

                    if ($allus_em_emails_allus_clientes_rel_row->num_rows > 0) {
                        while ($row = $allus_em_emails_allus_clientes_rel_row->fetch_assoc()) {
                            $allus_em_emails_allus_clientes_rel[] = $row;
                        }

                        foreach ($allus_em_emails_allus_clientes_rel as $key => $object) {
                            $allus_em_emails_allus_clientes_rel_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_emails_allus_clientes_rel');
                        }
                    }

                    //Depuración de allus_interacciones relacionados con Clientes
                    $allus_interacciones_row = $this->db->query("SELECT * FROM allus_interacciones WHERE inte_clie_id IN ('" . $allus_clientes_id . "')");
                    $allus_interacciones = [];
                    $allus_interacciones_id = [];

                    if ($allus_interacciones_row->num_rows > 0) {
                        while ($row = $allus_interacciones_row->fetch_assoc()) {
                            $allus_interacciones[] = $row;
                        }

                        foreach ($allus_interacciones as $key => $object) {
                            $allus_interacciones_id[] = $object['id'];
                            $this->createInsert($object, 'allus_interacciones');
                        }
                        $allus_interacciones_id = implode("', '", $allus_interacciones_id);
                        $this->deleteAuditGeneral($allus_interacciones_id, 'allus_interacciones');
                    }

                    //Depuración de allus_notas_credito_debito relacionados con Clientes
                    $allus_notas_credito_debito_row = $this->db->query("SELECT * FROM allus_notas_credito_debito WHERE id_cliente IN ('" . $allus_clientes_id . "')");
                    $allus_notas_credito_debito = [];
                    $allus_notas_credito_debito_id = [];

                    if ($allus_notas_credito_debito_row->num_rows > 0) {
                        while ($row = $allus_notas_credito_debito_row->fetch_assoc()) {
                            $allus_notas_credito_debito[] = $row;
                        }

                        foreach ($allus_notas_credito_debito as $key => $object) {
                            $allus_notas_credito_debito_id[] = $object['id'];
                            $this->createInsert($object, 'allus_notas_credito_debito');
                        }
                    }

                    if(empty($allus_notas_credito_debito_id) == false){
                        $allus_notas_credito_debito_id = implode("', '", $allus_notas_credito_debito_id);
                        //Depuración de allus_nc_adjuntos relacionados con NC
                        $allus_nc_adjuntos_row = $this->db->query("SELECT * FROM allus_nc_adjuntos WHERE allus_nc_id IN ('" . $allus_notas_credito_debito_id . "')");
                        $allus_nc_adjuntos = [];
                        $allus_nc_adjuntos_id = [];

                        if ($allus_nc_adjuntos_row->num_rows > 0) {
                            while ($row = $allus_nc_adjuntos_row->fetch_assoc()) {
                                $allus_nc_adjuntos[] = $row;
                            }

                            foreach ($allus_nc_adjuntos as $key => $object) {
                                $allus_nc_adjuntos_id[] = $object['id'];
                                $this->createInsert($object, 'allus_nc_adjuntos');
                            }
                        }

                        //Depuración de allus_nc_facturas relacionados con NC
                        $allus_nc_facturas_row = $this->db->query("SELECT * FROM allus_nc_facturas WHERE allus_nc_id IN ('" . $allus_notas_credito_debito_id . "')");
                        $allus_nc_facturas = [];
                        $allus_nc_facturas_id = [];

                        if ($allus_nc_facturas_row->num_rows > 0) {
                            while ($row = $allus_nc_facturas_row->fetch_assoc()) {
                                $allus_nc_facturas[] = $row;
                            }

                            foreach ($allus_nc_facturas as $key => $object) {
                                $allus_nc_facturas_id[] = $object['id'];
                                $this->createInsert($object, 'allus_nc_facturas');
                            }
                        }

                        //Depuración de allus_nc_gestiones relacionados con NC
                        $allus_nc_gestiones_row = $this->db->query("SELECT * FROM allus_nc_gestiones WHERE gest_nc_id IN ('" . $allus_notas_credito_debito_id . "')");
                        $allus_nc_gestiones = [];
                        $allus_nc_gestiones_id = [];

                        if ($allus_nc_gestiones_row->num_rows > 0) {
                            while ($row = $allus_nc_gestiones_row->fetch_assoc()) {
                                $allus_nc_gestiones[] = $row;
                            }

                            foreach ($allus_nc_facturas as $key => $object) {
                                $allus_nc_gestiones_id[] = $object['id'];
                                $this->createInsert($object, 'allus_nc_gestiones');
                            }
                        }

                        //Depuración de allus_nc_reasignacion relacionados con NC
                        $allus_nc_reasignacion_row = $this->db->query("SELECT * FROM allus_nc_reasignacion WHERE nota_id IN ('" . $allus_notas_credito_debito_id . "')");
                        $allus_nc_reasignacion = [];
                        $allus_nc_reasignacion_id = [];

                        if ($allus_nc_reasignacion_row->num_rows > 0) {
                            while ($row = $allus_nc_reasignacion_row->fetch_assoc()) {
                                $allus_nc_reasignacion[] = $row;
                            }

                            foreach ($allus_nc_reasignacion as $key => $object) {
                                $allus_nc_reasignacion_id[] = $object['id'];
                                $this->createInsert($object, 'allus_nc_reasignacion');
                            }
                        }

                        //Depuración de allus_workflow_notifications_log relacionados con NC
                        $allus_workflow_notifications_log_row = $this->db->query("SELECT * FROM allus_workflow_notifications_log WHERE nota_id IN ('" . $allus_notas_credito_debito_id . "')");
                        $allus_workflow_notifications_log = [];
                        $allus_workflow_notifications_log_id = [];

                        if ($allus_workflow_notifications_log_row->num_rows > 0) {
                            while ($row = $allus_workflow_notifications_log_row->fetch_assoc()) {
                                $allus_workflow_notifications_log[] = $row;
                            }

                            foreach ($allus_workflow_notifications_log as $key => $object) {
                                $allus_workflow_notifications_log_id[] = $object['id'];
                                $this->createInsert($object, 'allus_workflow_notifications_log');
                            }
                        }

                        //Depuración de lems_actions relacionados con NC
                        $lems_actions_row = $this->db->query("SELECT * FROM lems_actions WHERE ncnd_id IN ('" . $allus_notas_credito_debito_id . "')");
                        $lems_actions = [];
                        $lems_actions_id = [];

                        if ($lems_actions_row->num_rows > 0) {
                            while ($row = $lems_actions_row->fetch_assoc()) {
                                $lems_actions[] = $row;
                            }

                            foreach ($lems_actions as $key => $object) {
                                $lems_actions_id[] = $object['id'];
                                $this->createInsert($object, 'lems_actions');
                            }
                        }

                        //Depuración de allus_approval_management relacionados con NC
                        $allus_approval_management_row = $this->db->query("SELECT * FROM allus_approval_management WHERE id_notacredito IN ('" . $allus_notas_credito_debito_id . "')");
                        $allus_approval_management = [];
                        $allus_approval_management_id = [];

                        if ($allus_approval_management_row->num_rows > 0) {
                            while ($row = $allus_approval_management_row->fetch_assoc()) {
                                $allus_approval_management[] = $row;
                            }

                            foreach ($allus_approval_management as $key => $object) {
                                $allus_approval_management_id[] = $object['id'];
                                $this->createInsert($object, 'allus_approval_management');
                            }
                        }
                    }

                    //Depuración de allus_rm_cases relacionados con Clientes
                    $allus_rm_cases_row = $this->db->query("SELECT * FROM allus_rm_cases WHERE rmcas_clie_id IN ('" . $allus_clientes_id . "')");
                    $allus_rm_cases = [];
                    $allus_rm_cases_id = [];

                    if ($allus_rm_cases_row->num_rows > 0) {
                        while ($row = $allus_rm_cases_row->fetch_assoc()) {
                            $allus_rm_cases[] = $row;
                        }

                        foreach ($allus_rm_cases as $key => $object) {
                            $allus_rm_cases_id[] = $object['id'];
                            $this->createInsert($object, 'allus_rm_cases');
                        }
                    }

                    if(empty($allus_rm_cases_id) == false){
                        $allus_rm_cases_id = implode("', '", $allus_rm_cases_id);
                        //Depuración de allus_rm_cases relacionados con Clientes
                        $allus_rm_cases_managements_row = $this->db->query("SELECT * FROM allus_rm_cases_managements WHERE rm_cases_id IN ('" . $allus_rm_cases_id . "')");
                        $allus_rm_cases_managements = [];
                        $allus_rm_cases_managements_id = [];

                        if ($allus_rm_cases_managements_row->num_rows > 0) {
                            while ($row = $allus_rm_cases_managements_row->fetch_assoc()) {
                                $allus_rm_cases_managements[] = $row;
                            }

                            foreach ($allus_rm_cases_managements as $key => $object) {
                                $allus_rm_cases_managements_id[] = $object['id'];
                                $this->createInsert($object, 'allus_rm_cases_managements');
                            }
                        }
                    }

                    //Depuración de allus_sc_appointments relacionados con Clientes
                    $allus_sc_appointments_row = $this->db->query("SELECT * FROM allus_sc_appointments WHERE appo_clie_id IN ('" . $allus_clientes_id . "')");
                    $allus_sc_appointments = [];
                    $allus_sc_appointments_id = [];

                    if ($allus_sc_appointments_row->num_rows > 0) {
                        while ($row = $allus_sc_appointments_row->fetch_assoc()) {
                            $allus_sc_appointments[] = $row;
                        }

                        foreach ($allus_sc_appointments as $key => $object) {
                            $allus_sc_appointments_id[] = $object['id'];
                            $this->createInsert($object, '$allus_sc_appointments_id');
                        }

                        $allus_sc_appointments_id = implode("', '", $allus_sc_appointments_id);
                        $this->deleteParentsGeneral($allus_sc_appointments_id, 'allus_sc_appointments', 'appo_asse_id');
                        $this->deleteAuditGeneral($allus_sc_appointments_id, 'allus_sc_appointments');
                    }

                    //Depuración de hiq_requests relacionados con Clientes
                    $hiq_requests_row = $this->db->query("SELECT * FROM hiq_requests WHERE id_cliente IN ('" . $allus_clientes_id . "')");
                    $hiq_requests = [];
                    $hiq_requests_id = [];

                    if ($hiq_requests_row->num_rows > 0) {
                        while ($row = $hiq_requests_row->fetch_assoc()) {
                            $hiq_requests[] = $row;
                        }

                        foreach ($hiq_requests as $key => $object) {
                            $hiq_requests_id[] = $object['id'];
                            $this->createInsert($object, 'hiq_requests');
                        }
                    }

                    if(empty($hiq_requests_id) == false){
                        $hiq_requests_id = implode("', '", $hiq_requests_id);
                        //Depuración de hiq_attachments relacionados con Clientes
                        $hiq_attachments_row = $this->db->query("SELECT * FROM hiq_attachments WHERE hiq_id IN ('" . $hiq_requests_id . "')");
                        $hiq_attachments = [];
                        $hiq_attachments_id = [];

                        if ($hiq_attachments_row->num_rows > 0) {
                            while ($row = $hiq_attachments_row->fetch_assoc()) {
                                $hiq_attachments[] = $row;
                            }

                            foreach ($hiq_attachments as $key => $object) {
                                $hiq_attachments_id[] = $object['id'];
                                $this->createInsert($object, 'hiq_attachments');
                            }
                        }

                        //Depuración de hiq_commercial_information relacionados con Clientes
                        $hiq_commercial_information_row = $this->db->query("SELECT * FROM hiq_commercial_information WHERE hiq_id IN ('" . $hiq_requests_id . "')");
                        $hiq_commercial_information = [];
                        $hiq_commercial_information_id = [];

                        if ($hiq_commercial_information_row->num_rows > 0) {
                            while ($row = $hiq_commercial_information_row->fetch_assoc()) {
                                $hiq_commercial_information[] = $row;
                            }

                            foreach ($hiq_commercial_information as $key => $object) {
                                $hiq_commercial_information_id[] = $object['id'];
                                $this->createInsert($object, 'hiq_commercial_information');
                            }
                        }

                        //Depuración de hiq_commercial_information relacionados con Clientes
                        $hiq_managements_row = $this->db->query("SELECT * FROM hiq_managements WHERE hiq_id IN ('" . $hiq_requests_id . "')");
                        $hiq_managements = [];
                        $hiq_managements_id = [];

                        if ($hiq_managements_row->num_rows > 0) {
                            while ($row = $hiq_managements_row->fetch_assoc()) {
                                $hiq_managements[] = $row;
                            }

                            foreach ($hiq_managements as $key => $object) {
                                $hiq_managements_id[] = $object['id'];
                                $this->createInsert($object, 'hiq_managements');
                            }
                        }

                        //Depuración de hiq_traceability relacionados con Clientes
                        $hiq_traceability_row = $this->db->query("SELECT * FROM hiq_traceability WHERE id_hiq IN ('" . $hiq_requests_id . "')");
                        $hiq_traceability = [];
                        $hiq_traceability_id = [];

                        if ($hiq_traceability_row->num_rows > 0) {
                            while ($row = $hiq_traceability_row->fetch_assoc()) {
                                $hiq_traceability[] = $row;
                            }

                            foreach ($hiq_traceability as $key => $object) {
                                $hiq_traceability_id[] = $object['id'];
                                $this->createInsert($object, 'hiq_traceability');
                            }
                        }

                        //Depuración de hiq_traceability relacionados con Clientes
                        $hiq_workflow_notifications_log_row = $this->db->query("SELECT * FROM hiq_workflow_notifications_log WHERE hiq_id IN ('" . $hiq_requests_id . "')");
                        $hiq_workflow_notifications_log = [];
                        $hiq_workflow_notifications_log_id = [];

                        if ($hiq_workflow_notifications_log_row->num_rows > 0) {
                            while ($row = $hiq_workflow_notifications_log_row->fetch_assoc()) {
                                $hiq_workflow_notifications_log[] = $row;
                            }

                            foreach ($hiq_workflow_notifications_log as $key => $object) {
                                $hiq_workflow_notifications_log_id[] = $object['id'];
                                $this->createInsert($object, 'hiq_workflow_notifications_log');
                            }
                        }
                    }

                    //Depuración de lems_cases relacionados con Clientes
                    $lems_cases_row = $this->db->query("SELECT * FROM lems_cases WHERE client_id IN ('" . $allus_clientes_id . "')");
                    $lems_cases = [];
                    $lems_cases_id = [];

                    if ($lems_cases_row->num_rows > 0) {
                        while ($row = $lems_cases_row->fetch_assoc()) {
                            $lems_cases[] = $row;
                        }

                        foreach ($lems_cases as $key => $object) {
                            $lems_cases_id[] = $object['id'];
                            $this->createInsert($object, 'lems_cases');
                        }
                    }

                    if(empty($lems_cases_id) == false){
                        $lems_cases_id = implode("', '", $lems_cases_id);
                        //Depuración de lems_cases relacionados con Clientes
                        $lems_actions_row = $this->db->query("SELECT * FROM lems_actions WHERE lems_case_id IN ('" . $lems_cases_id . "')");
                        $lems_actions = [];
                        $lems_actions_id = [];

                        if ($lems_actions_row->num_rows > 0) {
                            while ($row = $lems_actions_row->fetch_assoc()) {
                                $lems_actions[] = $row;
                            }

                            foreach ($lems_actions as $key => $object) {
                                $lems_actions_id[] = $object['id'];
                                $this->createInsert($object, 'lems_actions');
                            }
                        }

                        //Depuración de lems_cases relacionados con Clientes
                        $allus_lems_cases_attachments_row = $this->db->query("SELECT * FROM allus_lems_cases_attachments WHERE lems_case_id IN ('" . $lems_cases_id . "')");
                        $allus_lems_cases_attachments = [];
                        $allus_lems_cases_attachments_id = [];

                        if ($allus_lems_cases_attachments_row->num_rows > 0) {
                            while ($row = $allus_lems_cases_attachments_row->fetch_assoc()) {
                                $allus_lems_cases_attachments[] = $row;
                            }

                            foreach ($allus_lems_cases_attachments as $key => $object) {
                                $allus_lems_cases_attachments_id[] = $object['id'];
                                $this->createInsert($object, 'allus_lems_cases_attachments');
                            }
                        }

                        $allus_lems_cases_managements_row = $this->db->query("SELECT * FROM allus_lems_cases_managements WHERE lems_case_id IN ('" . $lems_cases_id . "')");
                        $allus_lems_cases_managements = [];
                        $allus_lems_cases_managements_id = [];

                        if ($allus_lems_cases_managements_row->num_rows > 0) {
                            while ($row = $allus_lems_cases_managements_row->fetch_assoc()) {
                                $allus_lems_cases_managements[] = $row;
                            }

                            foreach ($allus_lems_cases_managements as $key => $object) {
                                $allus_lems_cases_managements_id[] = $object['id'];
                                $this->createInsert($object, 'allus_lems_cases_managements');
                            }
                        }

                    }

                    //Depuración de io_installation_orders relacionados con Clientes
                    $io_installation_orders_row = $this->db->query("SELECT * FROM io_installation_orders WHERE idclient IN ('" . $allus_clientes_id . "')");
                    $io_installation_orders = [];
                    $io_installation_orders_id = [];

                    if ($io_installation_orders_row->num_rows > 0) {
                        while ($row = $io_installation_orders_row->fetch_assoc()) {
                            $io_installation_orders[] = $row;
                        }

                        foreach ($io_installation_orders as $key => $object) {
                            $io_installation_orders_id[] = $object['id'];
                            $this->createInsert($object, 'oi_config_branchoffice');
                        }
                    }

                    if(empty($io_installation_orders_id) == false){
                        $io_installation_orders_id = implode("', '", $io_installation_orders_id);
                        //Depuración de io_managements relacionados con OI
                        $io_managements_row = $this->db->query("SELECT * FROM io_managements WHERE io_installation_orders_id IN ('" . $io_installation_orders_id . "')");
                        $io_managements = [];
                        $io_managements_id = [];

                        if ($io_managements_row->num_rows > 0) {
                            while ($row = $io_managements_row->fetch_assoc()) {
                                $io_managements[] = $row;
                            }

                            foreach ($io_managements as $key => $object) {
                                $io_managements_id[] = $object['id'];
                                $this->createInsert($object, 'io_managements');
                            }
                        }

                        //Depuración de io_products relacionados con OI
                        $io_products_row = $this->db->query("SELECT * FROM io_products WHERE io_installation_orders_id IN ('" . $io_installation_orders_id . "')");
                        $io_products = [];
                        $io_products_id = [];

                        if ($io_products_row->num_rows > 0) {
                            while ($row = $io_products_row->fetch_assoc()) {
                                $io_products[] = $row;
                            }

                            foreach ($io_products as $key => $object) {
                                $io_products_id[] = $object['id'];
                                $this->createInsert($object, 'io_products');
                            }
                        }

                        //Depuración de io_security relacionados con OI
                        $io_security_row = $this->db->query("SELECT * FROM io_security WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_security = [];
                        $io_security_id = [];

                        if ($io_security_row->num_rows > 0) {
                            while ($row = $io_security_row->fetch_assoc()) {
                                $io_security[] = $row;
                            }

                            foreach ($io_security as $key => $object) {
                                $io_security_id[] = $object['id'];
                                $this->createInsert($object, 'io_security');
                            }
                        }

                        //Depuración de io_accounting_report relacionados con OI
                        $io_accounting_report_row = $this->db->query("SELECT * FROM io_accounting_report WHERE id_order IN ('" . $io_installation_orders_id . "')");
                        $io_accounting_report = [];
                        $io_accounting_report_id = [];

                        if ($io_accounting_report_row->num_rows > 0) {
                            while ($row = $io_accounting_report_row->fetch_assoc()) {
                                $io_accounting_report[] = $row;
                            }

                            foreach ($io_accounting_report as $key => $object) {
                                $io_accounting_report_id[] = $object['id'];
                                $this->createInsert($object, 'io_accounting_report');
                            }
                        }

                        //Depuración de io_attachments relacionados con OI
                        $io_attachments_row = $this->db->query("SELECT * FROM io_attachments WHERE io_installation_orders_id IN ('" . $io_installation_orders_id . "')");
                        $io_attachments = [];
                        $io_attachments_id = [];

                        if ($io_attachments_row->num_rows > 0) {
                            while ($row = $io_attachments_row->fetch_assoc()) {
                                $io_attachments[] = $row;
                            }

                            foreach ($io_attachments as $key => $object) {
                                $io_attachments_id[] = $object['id'];
                                $this->createInsert($object, 'io_attachments');
                            }
                        }

                        //Depuración de io_billing relacionados con OI
                        $io_billing_row = $this->db->query("SELECT * FROM io_billing WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_billing = [];
                        $io_billing_id = [];

                        if ($io_billing_row->num_rows > 0) {
                            while ($row = $io_billing_row->fetch_assoc()) {
                                $io_billing[] = $row;
                            }

                            foreach ($io_billing as $key => $object) {
                                $io_billing_id[] = $object['id'];
                                $this->createInsert($object, 'io_billing');
                            }
                        }

                        //Depuración de io_business_evaluation relacionados con OI
                        $io_business_evaluation_row = $this->db->query("SELECT * FROM io_business_evaluation WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_business_evaluation = [];
                        $io_business_evaluation_id = [];

                        if ($io_business_evaluation_row->num_rows > 0) {
                            while ($row = $io_business_evaluation_row->fetch_assoc()) {
                                $io_business_evaluation[] = $row;
                            }

                            foreach ($io_business_evaluation as $key => $object) {
                                $io_business_evaluation_id[] = $object['id'];
                                $this->createInsert($object, 'io_business_evaluation');
                            }
                        }

                        if(empty($io_business_evaluation_id) == false){
                            $io_business_evaluation_id = implode("', '", $io_business_evaluation_id);
                            //Depuración de io_be_future_business relacionados con OI
                            $io_be_future_business_row = $this->db->query("SELECT * FROM io_be_future_business WHERE io_be_id IN ('" . $io_business_evaluation_id . "')");
                            $io_be_future_business = [];
                            $io_be_future_business_id = [];

                            if ($io_be_future_business_row->num_rows > 0) {
                                while ($row = $io_be_future_business_row->fetch_assoc()) {
                                    $io_be_future_business[] = $row;
                                }

                                foreach ($io_be_future_business as $key => $object) {
                                    $io_be_future_business_id[] = $object['id'];
                                    $this->createInsert($object, 'io_be_future_business');
                                }
                            }
                        }

                        //Depuración de io_business_evaluation relacionados con OI
                        $io_business_evaluation_row = $this->db->query("SELECT * FROM io_business_evaluation WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_business_evaluation = [];
                        $io_business_evaluation_id = [];

                        if ($io_business_evaluation_row->num_rows > 0) {
                            while ($row = $io_business_evaluation_row->fetch_assoc()) {
                                $io_business_evaluation[] = $row;
                            }

                            foreach ($io_business_evaluation as $key => $object) {
                                $io_business_evaluation_id[] = $object['id'];
                                $this->createInsert($object, 'io_business_evaluation');
                            }
                        }

                        if(empty($io_business_evaluation_id) == false){
                            $io_business_evaluation_id = implode("', '", $io_business_evaluation_id);
                            //Depuración de io_business_evaluation_attachments relacionados con OI
                            $io_business_evaluation_attachments_row = $this->db->query("SELECT * FROM io_business_evaluation_attachments WHERE io_business_evaluation_id IN ('" . $io_business_evaluation_id . "')");
                            $io_business_evaluation_attachments = [];
                            $io_business_evaluation_attachments_id = [];

                            if ($io_business_evaluation_attachments_row->num_rows > 0) {
                                while ($row = $io_business_evaluation_attachments_row->fetch_assoc()) {
                                    $io_business_evaluation_attachments[] = $row;
                                }

                                foreach ($io_business_evaluation_attachments as $key => $object) {
                                    $io_business_evaluation_attachments_id[] = $object['id'];
                                    $this->createInsert($object, 'io_business_evaluation_attachments');
                                }
                            }
                        }

                        //Depuración de io_closing_requests relacionados con OI
                        $io_closing_requests_row = $this->db->query("SELECT * FROM io_closing_requests WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_closing_requests = [];
                        $io_closing_requests_id = [];

                        if ($io_closing_requests_row->num_rows > 0) {
                            while ($row = $io_closing_requests_row->fetch_assoc()) {
                                $io_closing_requests[] = $row;
                            }

                            foreach ($io_closing_requests as $key => $object) {
                                $io_closing_requests_id[] = $object['id'];
                                $this->createInsert($object, 'io_closing_requests');
                            }
                        }

                        if(empty($io_closing_requests_id) == false){
                            $io_closing_requests_id = implode("', '", $io_closing_requests_id);

                            //Depuración de io_closing_request_attachments relacionados con OI
                            $io_closing_request_attachments_row = $this->db->query("SELECT * FROM io_closing_request_attachments WHERE io_closing_request_id IN ('" . $io_closing_requests_id . "')");
                            $io_closing_request_attachments = [];
                            $io_closing_request_attachments_id = [];

                            if ($io_closing_request_attachments_row->num_rows > 0) {
                                while ($row = $io_closing_request_attachments_row->fetch_assoc()) {
                                    $io_closing_request_attachments[] = $row;
                                }

                                foreach ($io_closing_request_attachments as $key => $object) {
                                    $io_closing_request_attachments_id[] = $object['id'];
                                    $this->createInsert($object, 'io_closing_request_attachments');
                                }
                            }
                        }

                        //Depuración de io_commercial relacionados con OI
                        $io_commercial_row = $this->db->query("SELECT * FROM io_commercial WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_commercial = [];
                        $io_commercial_id = [];

                        if ($io_commercial_row->num_rows > 0) {
                            while ($row = $io_commercial_row->fetch_assoc()) {
                                $io_commercial[] = $row;
                            }

                            foreach ($io_commercial as $key => $object) {
                                $io_commercial_id[] = $object['id'];
                                $this->createInsert($object, 'io_commercial');
                            }
                        }

                        //Depuración de io_costs relacionados con OI
                        $io_costs_row = $this->db->query("SELECT * FROM io_costs WHERE io_installation_orders_id IN ('" . $io_installation_orders_id . "')");
                        $io_costs = [];
                        $io_costs_id = [];

                        if ($io_costs_row->num_rows > 0) {
                            while ($row = $io_costs_row->fetch_assoc()) {
                                $io_costs[] = $row;
                            }

                            foreach ($io_costs as $key => $object) {
                                $io_costs_id[] = $object['id'];
                                $this->createInsert($object, 'io_costs');
                            }
                        }

                        if(empty($io_costs_id) == false){
                            $io_costs_id = implode("', '", $io_costs_id);
                            //Depuración de io_expenses relacionados con OI
                            $io_expenses_row = $this->db->query("SELECT * FROM io_expenses WHERE io_costs_id IN ('" . $io_costs_id . "')");
                            $io_expenses = [];
                            $io_expenses_id = [];

                            if ($io_expenses_row->num_rows > 0) {
                                while ($row = $io_expenses_row->fetch_assoc()) {
                                    $io_expenses[] = $row;
                                }

                                foreach ($io_expenses as $key => $object) {
                                    $io_expenses_id[] = $object['id'];
                                    $this->createInsert($object, 'io_expenses');
                                }
                            }

                            //Depuración de io_manpower_contractor relacionados con OI
                            $io_manpower_contractor_row = $this->db->query("SELECT * FROM io_manpower_contractor WHERE io_costs_id IN ('" . $io_costs_id . "')");
                            $io_manpower_contractor = [];
                            $io_manpower_contractor_id = [];

                            if ($io_manpower_contractor_row->num_rows > 0) {
                                while ($row = $io_manpower_contractor_row->fetch_assoc()) {
                                    $io_manpower_contractor[] = $row;
                                }

                                foreach ($io_manpower_contractor as $key => $object) {
                                    $io_manpower_contractor_id[] = $object['id'];
                                    $this->createInsert($object, 'io_manpower_contractor');
                                }
                            }

                            //Depuración de io_manpower_linde relacionados con OI
                            $io_manpower_linde_row = $this->db->query("SELECT * FROM io_manpower_linde WHERE io_costs_id IN ('" . $io_costs_id . "')");
                            $io_manpower_linde = [];
                            $io_manpower_linde_id = [];

                            if ($io_manpower_linde_row->num_rows > 0) {
                                while ($row = $io_manpower_linde_row->fetch_assoc()) {
                                    $io_manpower_linde[] = $row;
                                }

                                foreach ($io_manpower_linde as $key => $object) {
                                    $io_manpower_linde_id[] = $object['id'];
                                    $this->createInsert($object, 'io_manpower_linde');
                                }
                            }

                            //Depuración de io_materials_costs relacionados con OI
                            $io_materials_costs_row = $this->db->query("SELECT * FROM io_materials_costs WHERE io_costs_id IN ('" . $io_costs_id . "')");
                            $io_materials_costs = [];
                            $io_materials_costs_id = [];

                            if ($io_materials_costs_row->num_rows > 0) {
                                while ($row = $io_materials_costs_row->fetch_assoc()) {
                                    $io_materials_costs[] = $row;
                                }

                                foreach ($io_materials_costs as $key => $object) {
                                    $io_materials_costs_id[] = $object['id'];
                                    $this->createInsert($object, 'io_materials_costs');
                                }
                            }

                            //Depuración de io_total_budget relacionados con OI
                            $io_total_budget_row = $this->db->query("SELECT * FROM io_total_budget WHERE io_costs_id IN ('" . $io_costs_id . "')");
                            $io_total_budget = [];
                            $io_total_budget_id = [];

                            if ($io_total_budget_row->num_rows > 0) {
                                while ($row = $io_total_budget_row->fetch_assoc()) {
                                    $io_total_budget[] = $row;
                                }

                                foreach ($io_total_budget as $key => $object) {
                                    $io_total_budget_id[] = $object['id'];
                                    $this->createInsert($object, 'io_total_budget');
                                }
                            }

                        }

                        //Depuración de io_costs relacionados con OI
                        $io_document_budget_row = $this->db->query("SELECT * FROM io_document_budget WHERE io_id IN ('" . $io_installation_orders_id . "')");
                        $io_document_budget = [];
                        $io_document_budget_id = [];

                        if ($io_document_budget_row->num_rows > 0) {
                            while ($row = $io_document_budget_row->fetch_assoc()) {
                                $io_document_budget[] = $row;
                            }

                            foreach ($io_document_budget as $key => $object) {
                                $io_document_budget_id[] = $object['id'];
                                $this->createInsert($object, 'io_costs');
                            }
                        }

                        //Depuración de io_installation relacionados con OI
                        $io_installation_row = $this->db->query("SELECT * FROM io_installation WHERE io_installation_orders_id IN ('" . $io_installation_orders_id . "')");
                        $io_installation = [];
                        $io_installation_id = [];

                        if ($io_installation_row->num_rows > 0) {
                            while ($row = $io_installation_row->fetch_assoc()) {
                                $io_installation[] = $row;
                            }

                            foreach ($io_installation as $key => $object) {
                                $io_installation_id[] = $object['id'];
                                $this->createInsert($object, 'io_costs');
                            }
                        }
                    }

                    //Depuración de oi_config_branchoffice relacionados con Clientes
                    $oi_config_branchoffice_row = $this->db->query("SELECT * FROM oi_config_branchoffice WHERE client_id IN ('" . $allus_clientes_id . "')");
                    $oi_config_branchoffice = [];
                    $oi_config_branchoffice_id = [];

                    if ($oi_config_branchoffice_row->num_rows > 0) {
                        while ($row = $oi_config_branchoffice_row->fetch_assoc()) {
                            $oi_config_branchoffice[] = $row;
                        }

                        foreach ($oi_config_branchoffice as $key => $object) {
                            $oi_config_branchoffice_id[] = $object['id'];
                            $this->createInsert($object, 'oi_config_branchoffice');
                        }
                    }

                    //Depuración de tc_technical_service relacionados con Clientes
                    $tc_technical_service_row = $this->db->query("SELECT * FROM tc_technical_service WHERE id_cliente IN ('" . $allus_clientes_id . "')");
                    $tc_technical_service = [];
                    $tc_technical_service_id = [];

                    if ($tc_technical_service_row->num_rows > 0) {
                        while ($row = $tc_technical_service_row->fetch_assoc()) {
                            $tc_technical_service[] = $row;
                        }

                        foreach ($tc_technical_service as $key => $object) {
                            $tc_technical_service_id[] = $object['id'];
                            $this->createInsert($object, 'tc_technical_service');
                        }
                    }

                    if(empty($tc_technical_service_id) == false){
                        $tc_technical_service_id = implode("', '", $tc_technical_service_id);

                        $tc_attachments_row = $this->db->query("SELECT * FROM tc_attachments WHERE id_technical_service IN ('" . $tc_technical_service_id . "')");
                        $tc_attachments = [];
                        $tc_attachments_id = [];

                        if ($tc_attachments_row->num_rows > 0) {
                            while ($row = $tc_attachments_row->fetch_assoc()) {
                                $tc_attachments[] = $row;
                            }

                            foreach ($tc_attachments as $key => $object) {
                                $tc_attachments_id[] = $object['id'];
                                $this->createInsert($object, 'tc_attachments');
                            }
                        }

                        $tc_budget_execution_row = $this->db->query("SELECT * FROM tc_budget_execution WHERE id_technical_service IN ('" . $tc_technical_service_id . "')");
                        $tc_budget_execution = [];
                        $tc_budget_execution_id = [];

                        if ($tc_budget_execution_row->num_rows > 0) {
                            while ($row = $tc_budget_execution_row->fetch_assoc()) {
                                $tc_budget_execution[] = $row;
                            }

                            foreach ($tc_budget_execution as $key => $object) {
                                $tc_budget_execution_id[] = $object['id'];
                                $this->createInsert($object, 'tc_budget_execution');
                            }
                        }

                        $tc_cases_state_managements_aud_row = $this->db->query("SELECT * FROM tc_cases_state_managements_aud WHERE id_case IN ('" . $tc_technical_service_id . "')");
                        $tc_cases_state_managements_aud = [];
                        $tc_cases_state_managements_aud_id = [];

                        if ($tc_cases_state_managements_aud_row->num_rows > 0) {
                            while ($row = $tc_cases_state_managements_aud_row->fetch_assoc()) {
                                $tc_cases_state_managements_aud[] = $row;
                            }

                            foreach ($tc_cases_state_managements_aud as $key => $object) {
                                $tc_cases_state_managements_aud_id[] = $object['id'];
                                $this->createInsert($object, 'tc_cases_state_managements_aud');
                            }
                        }

                        $tc_mail_notification_row = $this->db->query("SELECT * FROM tc_mail_notification WHERE id_technical_service IN ('" . $tc_technical_service_id . "')");
                        $tc_mail_notification = [];
                        $tc_mail_notification_id = [];

                        if ($tc_mail_notification_row->num_rows > 0) {
                            while ($row = $tc_mail_notification_row->fetch_assoc()) {
                                $tc_mail_notification[] = $row;
                            }

                            foreach ($tc_mail_notification as $key => $object) {
                                $tc_mail_notification_id[] = $object['id'];
                                $this->createInsert($object, 'tc_mail_notification');
                            }
                        }

                        $tc_service_execution_row = $this->db->query("SELECT * FROM tc_service_execution WHERE id_technical_service IN ('" . $tc_technical_service_id . "')");
                        $tc_service_execution = [];
                        $tc_service_execution_id = [];

                        if ($tc_service_execution_row->num_rows > 0) {
                            while ($row = $tc_service_execution_row->fetch_assoc()) {
                                $tc_service_execution[] = $row;
                            }

                            foreach ($tc_service_execution as $key => $object) {
                                $tc_service_execution_id[] = $object['id'];
                                $this->createInsert($object, 'tc_service_execution');
                            }
                        }

                        if(empty($tc_service_execution_id) == false){
                            $tc_service_execution_id = implode("', '", $tc_service_execution_id);

                            $tc_log_execution_st_row = $this->db->query("SELECT * FROM tc_log_execution_st WHERE id_execution_service IN ('" . $tc_service_execution_id . "')");
                            $tc_log_execution_st = [];
                            $tc_log_execution_st_id = [];

                            if ($tc_log_execution_st_row->num_rows > 0) {
                                while ($row = $tc_log_execution_st_row->fetch_assoc()) {
                                    $tc_log_execution_st[] = $row;
                                }

                                foreach ($tc_log_execution_st as $key => $object) {
                                    $tc_log_execution_st_id[] = $object['id'];
                                    $this->createInsert($object, 'tc_log_execution_st');
                                }
                            }

                            $tc_pending_row = $this->db->query("SELECT * FROM tc_pending WHERE id_service_execution IN ('" . $tc_service_execution_id . "')");
                            $tc_pending = [];
                            $tc_pending_id = [];

                            if ($tc_pending_row->num_rows > 0) {
                                while ($row = $tc_pending_row->fetch_assoc()) {
                                    $tc_pending[] = $row;
                                }

                                foreach ($tc_pending as $key => $object) {
                                    $tc_pending_id[] = $object['id'];
                                    $this->createInsert($object, 'tc_pending');
                                }
                            }
                        }

                        $tc_technical_invoice_row = $this->db->query("SELECT * FROM tc_technical_invoice WHERE id_technical_service IN ('" . $tc_technical_service_id . "')");
                        $tc_technical_invoice = [];
                        $tc_technical_invoice_id = [];

                        if ($tc_technical_invoice_row->num_rows > 0) {
                            while ($row = $tc_technical_invoice_row->fetch_assoc()) {
                                $tc_technical_invoice[] = $row;
                            }

                            foreach ($tc_technical_invoice as $key => $object) {
                                $tc_technical_invoice_id[] = $object['id'];
                                $this->createInsert($object, 'tc_technical_invoice');
                            }
                        }

                        $ts_costs_row = $this->db->query("SELECT * FROM ts_costs WHERE id_technical_services IN ('" . $tc_technical_service_id . "')");
                        $ts_costs = [];
                        $ts_costs_id = [];

                        if ($ts_costs_row->num_rows > 0) {
                            while ($row = $ts_costs_row->fetch_assoc()) {
                                $ts_costs[] = $row;
                            }

                            foreach ($ts_costs as $key => $object) {
                                $ts_costs_id[] = $object['id'];
                                $this->createInsert($object, 'ts_costs');
                            }
                        }
                    }

                    //Depuración de allus_casos relacionados con Clientes
                    $allus_casos_row = $this->db->query("SELECT * FROM allus_casos WHERE caso_clie_id IN ('" . $allus_clientes_id . "')");
                    $allus_casos = [];
                    $allus_casos_id = [];

                    if ($allus_casos_row->num_rows > 0) {
                        while ($row = $allus_casos_row->fetch_assoc()) {
                            $allus_casos[] = $row;
                        }

                        foreach ($allus_casos as $key => $object) {
                            $allus_casos_id[] = $object['id'];
                            $this->createInsert($object, 'allus_casos');
                        }

                        $allus_casos_id = implode("', '", $allus_casos_id);
                        $this->deleteAuditGeneral($allus_casos_id, 'allus_casos');
                    }

                    if(empty($allus_casos_id) == false){
                        $allus_casos_attachments_row = $this->db->query("SELECT * FROM allus_casos_attachments WHERE caso_id IN ('" . $allus_casos_id . "')");
                        $allus_casos_attachments = [];
                        $allus_casos_attachments_id = [];

                        if ($allus_casos_attachments_row->num_rows > 0) {
                            while ($row = $allus_casos_attachments_row->fetch_assoc()) {
                                $allus_casos_attachments[] = $row;
                            }

                            foreach ($allus_casos_attachments as $key => $object) {
                                $allus_casos_attachments_id[] = $object['id'];
                                $this->createInsert($object, 'allus_casos_attachments');
                            }
                        }

                        $allus_is_cotizaciones_row = $this->db->query("SELECT * FROM allus_is_cotizaciones WHERE coti_caso_id IN ('" . $allus_casos_id . "')");
                        $allus_is_cotizaciones = [];
                        $allus_is_cotizaciones_id = [];

                        if ($allus_is_cotizaciones_row->num_rows > 0) {
                            while ($row = $allus_is_cotizaciones_row->fetch_assoc()) {
                                $allus_is_cotizaciones[] = $row;
                            }

                            foreach ($allus_is_cotizaciones as $key => $object) {
                                $allus_is_cotizaciones_id[] = $object['id'];
                                $this->createInsert($object, 'allus_is_cotizaciones');
                            }
                        }

                        if(empty($allus_is_cotizaciones_id) == false){
                            $allus_is_cotizaciones_id = implode("', '", $allus_is_cotizaciones_id);

                            $allus_is_cotizaciones_gestiones_row = $this->db->query("SELECT * FROM allus_is_cotizaciones_gestiones WHERE coge_coti_id IN ('" . $allus_is_cotizaciones_id . "')");
                            $allus_is_cotizaciones_gestiones = [];
                            $allus_is_cotizaciones_gestiones_id = [];

                            if ($allus_is_cotizaciones_gestiones_row->num_rows > 0) {
                                while ($row = $allus_is_cotizaciones_gestiones_row->fetch_assoc()) {
                                    $allus_is_cotizaciones_gestiones[] = $row;
                                }

                                foreach ($allus_is_cotizaciones_gestiones as $key => $object) {
                                    $allus_is_cotizaciones_gestiones_id[] = $object['id'];
                                    $this->createInsert($object, 'allus_is_cotizaciones_gestiones');
                                }
                            }
                        }

                        $allus_le_actividades_row = $this->db->query("SELECT * FROM allus_le_actividades WHERE acti_caso_id IN ('" . $allus_casos_id . "')");
                        $allus_le_actividades = [];
                        $allus_le_actividades_id = [];

                        if ($allus_le_actividades_row->num_rows > 0) {
                            while ($row = $allus_le_actividades_row->fetch_assoc()) {
                                $allus_le_actividades[] = $row;
                            }

                            foreach ($allus_le_actividades as $key => $object) {
                                $allus_le_actividades_id[] = $object['id'];
                                $this->createInsert($object, 'allus_le_actividades');
                            }

                            $allus_le_actividades_id = implode("', '", $allus_le_actividades_id);
                            $this->deleteAuditGeneral($allus_le_actividades_id, 'allus_le_actividades');
                        }

                        if(empty($allus_le_actividades_id) == false){
                            $allus_le_gestiones_row = $this->db->query("SELECT * FROM allus_le_gestiones WHERE gest_acti_id IN ('" . $allus_le_actividades_id . "')");
                            $allus_le_gestiones = [];
                            $allus_le_gestiones_id = [];

                            if ($allus_le_gestiones_row->num_rows > 0) {
                                while ($row = $allus_le_gestiones_row->fetch_assoc()) {
                                    $allus_le_gestiones[] = $row;
                                }

                                foreach ($allus_le_gestiones as $key => $object) {
                                    $allus_le_gestiones_id[] = $object['id'];
                                    $this->createInsert($object, 'allus_le_gestiones');
                                }

                                $allus_le_gestiones_id = implode("', '", $allus_le_gestiones_id);
                                $this->deleteAuditGeneral($allus_le_gestiones_id, 'allus_le_gestiones');
                            }
                        }

                        $allus_le_actividades_asesor_row = $this->db->query("SELECT * FROM allus_le_actividades_asesor WHERE acas_caso_id IN ('" . $allus_casos_id . "')");
                        $allus_le_actividades_asesor = [];
                        $allus_le_actividades_asesor_id = [];

                        if ($allus_le_actividades_asesor_row->num_rows > 0) {
                            while ($row = $allus_le_actividades_asesor_row->fetch_assoc()) {
                                $allus_le_actividades_asesor[] = $row;
                            }

                            foreach ($allus_le_actividades_asesor as $key => $object) {
                                $allus_le_actividades_asesor_id[] = $object['id'];
                                $this->createInsert($object, 'allus_le_actividades_asesor');
                            }
                        }
                    }
                }

                //Depuración de allus_em_templates relacionados con Países
                $allus_em_templates_row = $this->db->query("SELECT * FROM allus_em_templates WHERE pais_id IN ('" . $paises_id . "')");
                $allus_em_templates = [];
                $allus_em_templates_id = [];

                if ($allus_em_templates_row->num_rows > 0) {
                    while ($row = $allus_em_templates_row->fetch_assoc()) {
                        $allus_em_templates[] = $row;
                    }

                    foreach ($allus_em_templates as $key => $object) {
                        $allus_em_templates_id[] = $object['id'];
                        $this->createInsert($object, 'allus_em_templates');
                    }
                }

                if(empty($allus_em_templates_id) == false){
                    $allus_em_templates_id = implode("', '", $allus_em_templates_id);
                    //Depuración de allus_escalamientos relacionados con Los templates
                    $allus_escalamientos_row = $this->db->query("SELECT * FROM allus_escalamientos WHERE esca_emailtemplate_id IN ('" . $allus_em_templates_id . "')");
                    $allus_escalamientos = [];
                    $allus_escalamientos_id = [];

                    if ($allus_escalamientos_row->num_rows > 0) {
                        while ($row = $allus_escalamientos_row->fetch_assoc()) {
                            $allus_escalamientos[] = $row;
                        }

                        foreach ($allus_escalamientos as $key => $object) {
                            $allus_escalamientos_id[] = $object['id'];
                            $this->createInsert($object, 'allus_escalamientos');
                        }

                        $allus_escalamientos_id = implode("', '", $allus_escalamientos_id);
                        $this->deleteAuditGeneral($allus_escalamientos_id, 'allus_escalamientos');
                    }

                    //Depuración de allus_escalamientos relacionados con Los templates
                    $allus_le_paises_emailtemplates_row = $this->db->query("SELECT * FROM allus_le_paises_emailtemplates WHERE paem_emtp_id IN ('" . $allus_em_templates_id . "')");
                    $allus_le_paises_emailtemplates = [];
                    $allus_le_paises_emailtemplates_id = [];

                    if ($allus_le_paises_emailtemplates_row->num_rows > 0) {
                        while ($row = $allus_le_paises_emailtemplates_row->fetch_assoc()) {
                            $allus_le_paises_emailtemplates[] = $row;
                        }

                        foreach ($allus_le_paises_emailtemplates as $key => $object) {
                            $allus_le_paises_emailtemplates_id[] = $object['id'];
                            $this->createInsert($object, 'allus_le_paises_emailtemplates');
                        }

                        $allus_le_paises_emailtemplates_id = implode("', '", $allus_le_paises_emailtemplates_id);
                        $this->deleteAuditGeneral($allus_le_paises_emailtemplates_id, 'allus_le_paises_emailtemplates');
                    }
                }

                //Depuración de allus_grupos relacionados con Países
                $allus_grupos_row = $this->db->query("SELECT * FROM allus_grupos WHERE grou_pais_id IN ('" . $paises_id . "')");
                $allus_grupos = [];
                $allus_grupos_id = [];

                if ($allus_grupos_row->num_rows > 0) {
                    while ($row = $allus_grupos_row->fetch_assoc()) {
                        $allus_grupos[] = $row;
                    }

                    foreach ($allus_grupos as $key => $object) {
                        $allus_grupos_id[] = $object['id'];
                        $this->createInsert($object, 'allus_grupos');
                    }
                }

                if(empty($allus_grupos_id) == false){
                    $allus_grupos_id = implode("', '", $allus_grupos_id);
                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $allus_grupos_lider_rel_row = $this->db->query("SELECT * FROM allus_grupos_lider_rel WHERE grupo_id IN ('" . $allus_grupos_id . "')");
                    $allus_grupos_lider_rel = [];
                    $allus_grupos_lider_rel_id = [];

                    if ($allus_grupos_lider_rel_row->num_rows > 0) {
                        while ($row = $allus_grupos_lider_rel_row->fetch_assoc()) {
                            $allus_grupos_lider_rel[] = $row;
                        }

                        foreach ($allus_grupos_lider_rel as $key => $object) {
                            $allus_grupos_lider_rel_id[] = $object['id'];
                            $this->createInsert($object, 'allus_grupos_lider_rel');
                        }
                    }

                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $allus_rm_level_activites_row = $this->db->query("SELECT * FROM allus_rm_level_activites WHERE group_id IN ('" . $allus_grupos_id . "')");
                    $allus_rm_level_activites = [];
                    $allus_rm_level_activites_id = [];

                    if ($allus_rm_level_activites_row->num_rows > 0) {
                        while ($row = $allus_rm_level_activites_row->fetch_assoc()) {
                            $allus_rm_level_activites[] = $row;
                        }

                        foreach ($allus_rm_level_activites as $key => $object) {
                            $allus_rm_level_activites_id[] = $object['id'];
                            $this->createInsert($object, 'allus_rm_level_activites');
                        }
                    }

                    //Depuración de allus_em_emails_alerts relacionados con Grupos
                    $allus_em_emails_alerts_row = $this->db->query("SELECT * FROM allus_em_emails_alerts WHERE grupo_id IN ('" . $allus_grupos_id . "')");
                    $allus_em_emails_alerts = [];
                    $allus_em_emails_alerts_id = [];

                    if ($allus_em_emails_alerts_row->num_rows > 0) {
                        while ($row = $allus_em_emails_alerts_row->fetch_assoc()) {
                            $allus_em_emails_alerts[] = $row;
                        }

                        foreach ($allus_em_emails_alerts as $key => $object) {
                            $allus_em_emails_alerts_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_emails_alerts');
                        }


                    }

                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $users_allus_grupos_row = $this->db->query("SELECT * FROM users_allus_grupos WHERE allus_grupo_id IN ('" . $allus_grupos_id . "')");
                    $users_allus_grupos_rel = [];
                    $users_allus_grupos_id = [];

                    if ($users_allus_grupos_row->num_rows > 0) {
                        while ($row = $users_allus_grupos_row->fetch_assoc()) {
                            $users_allus_grupos_rel[] = $row;
                        }

                        foreach ($users_allus_grupos_rel as $key => $object) {
                            $users_allus_grupos_id[] = $object['id'];
                            $this->createInsert($object, 'users_allus_grupos');
                        }
                    }

                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $allus_em_areas_escalamiento_email_row = $this->db->query("SELECT * FROM allus_em_areas_escalamiento_email WHERE em_email_id IN ('" . $allus_grupos_id . "')");
                    $allus_em_areas_escalamiento_email = [];
                    $allus_em_areas_escalamiento_email_id = [];

                    if ($allus_em_areas_escalamiento_email_row->num_rows > 0) {
                        while ($row = $allus_em_areas_escalamiento_email_row->fetch_assoc()) {
                            $allus_em_areas_escalamiento_email[] = $row;
                        }

                        foreach ($allus_em_areas_escalamiento_email as $key => $object) {
                            $allus_em_areas_escalamiento_email_id[] = $object['id'];
                            $this->createInsert($object, 'allus_em_areas_escalamiento_email');
                        }
                    }
                }

                //Depuración de allus_holidays relacionados con Países
                $allus_holidays_row = $this->db->query("SELECT * FROM allus_holidays WHERE country_id IN ('" . $paises_id . "')");
                $allus_holidays = [];
                $allus_holidays_id = [];

                if ($allus_holidays_row->num_rows > 0) {
                    while ($row = $allus_holidays_row->fetch_assoc()) {
                        $allus_holidays[] = $row;
                    }

                    foreach ($allus_holidays as $key => $object) {
                        $allus_holidays_id[] = $object['id'];
                        $this->createInsert($object, 'allus_holidays');
                    }
                }

                //Depuración de allus_le_arbol_actividades relacionados con Países
                $allus_le_arbol_actividades_row = $this->db->query("SELECT * FROM allus_le_arbol_actividades WHERE arac_pais_id IN ('" . $paises_id . "')");
                $allus_le_arbol_actividades = [];
                $allus_le_arbol_actividades_id = [];

                if ($allus_le_arbol_actividades_row->num_rows > 0) {
                    while ($row = $allus_le_arbol_actividades_row->fetch_assoc()) {
                        $allus_le_arbol_actividades[] = $row;
                    }

                    foreach ($allus_le_arbol_actividades as $key => $object) {
                        $allus_le_arbol_actividades_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_arbol_actividades');
                    }

                    $allus_le_arbol_actividades_id = implode("', '", $allus_le_arbol_actividades_id);
                    $this->deleteAuditGeneral($allus_le_arbol_actividades_id, 'allus_le_arbol_actividades');
                }

                if(empty($allus_le_arbol_actividades_id) == false){
                    //Depuración de allus_le_arbol_actividades_escalamientos relacionados con Países
                    $allus_le_arbol_actividades_escalamientos_row = $this->db->query("SELECT * FROM allus_le_arbol_actividades_escalamientos WHERE araces_arac_id IN ('" . $allus_le_arbol_actividades_id . "')");
                    $allus_le_arbol_actividades_escalamientos = [];
                    $allus_le_arbol_actividades_escalamientos_id = [];

                    if ($allus_le_arbol_actividades_escalamientos_row->num_rows > 0) {
                        while ($row = $allus_le_arbol_actividades_escalamientos_row->fetch_assoc()) {
                            $allus_le_arbol_actividades_escalamientos[] = $row;
                        }

                        foreach ($allus_le_arbol_actividades_escalamientos as $key => $object) {
                            $allus_le_arbol_actividades_escalamientos_id[] = $object['id'];
                            $this->createInsert($object, 'allus_le_arbol_actividades_escalamientos');
                        }

                        $allus_le_arbol_actividades_escalamientos_id = implode("', '", $allus_le_arbol_actividades_escalamientos_id);
                        $this->deleteAuditGeneral($allus_le_arbol_actividades_escalamientos_id, 'allus_le_arbol_actividades_escalamientos');
                    }

                    //Depuración de allus_le_arbol_actividades_escalamientos relacionados con Países
                    $allus_le_actividades_row = $this->db->query("SELECT * FROM allus_le_actividades WHERE acti_baseactvi_id IN ('" . $allus_le_arbol_actividades_id . "')");
                    $allus_le_actividades = [];
                    $allus_le_actividades_id = [];

                    if ($allus_le_actividades_row->num_rows > 0) {
                        while ($row = $allus_le_actividades_row->fetch_assoc()) {
                            $allus_le_actividades[] = $row;
                        }

                        foreach ($allus_le_actividades as $key => $object) {
                            $allus_le_actividades_id[] = $object['id'];
                            $this->createInsert($object, 'allus_le_actividades');
                        }
                    }

                    //Depuración de allus_le_actividades_asesor relacionados con Países
                    $allus_le_actividades_asesor_row = $this->db->query("SELECT * FROM allus_le_actividades_asesor WHERE acas_baseactvi_id IN ('" . $allus_le_arbol_actividades_id . "')");
                    $allus_le_actividades_asesor = [];
                    $allus_le_actividades_asesor_id = [];

                    if ($allus_le_actividades_asesor_row->num_rows > 0) {
                        while ($row = $allus_le_actividades_asesor_row->fetch_assoc()) {
                            $allus_le_actividades_asesor[] = $row;
                        }

                        foreach ($allus_le_actividades_asesor as $key => $object) {
                            $allus_le_actividades_asesor_id[] = $object['id'];
                            $this->createInsert($object, 'allus_le_actividades_asesor');
                        }

                        $allus_le_actividades_asesor_id = implode("', '", $allus_le_actividades_asesor_id);
                        $this->deleteAuditGeneral($allus_le_actividades_asesor_id, 'allus_le_actividades_asesor');
                    }
                }

                //Depuración de branch_offices relacionados con Países
                $branch_offices_row = $this->db->query("SELECT * FROM branch_offices WHERE id_country IN ('" . $paises_id . "')");
                $branch_offices = [];
                $branch_offices_id = [];

                if ($branch_offices_row->num_rows > 0) {
                    while ($row = $branch_offices_row->fetch_assoc()) {
                        $branch_offices[] = $row;
                    }

                    foreach ($branch_offices as $key => $object) {
                        $branch_offices_id[] = $object['id'];
                        $this->createInsert($object, 'branch_offices');
                    }
                }

                if(empty($branch_offices_id) == false){
                    $branch_offices_id = implode("', '", $branch_offices_id);
                    //Depuración de doa_flow_levels_ranges relacionados con Países
                    $doa_flow_levels_ranges_row = $this->db->query("SELECT * FROM doa_flow_levels_ranges WHERE branch_office IN ('" . $branch_offices_id . "')");
                    $doa_flow_levels_ranges = [];
                    $doa_flow_levels_ranges_id = [];

                    if ($doa_flow_levels_ranges_row->num_rows > 0) {
                        while ($row = $doa_flow_levels_ranges_row->fetch_assoc()) {
                            $doa_flow_levels_ranges[] = $row;
                        }

                        foreach ($doa_flow_levels_ranges as $key => $object) {
                            $doa_flow_levels_ranges_id[] = $object['id'];
                            $this->createInsert($object, 'doa_flow_levels_ranges');
                        }
                    }
                }

                //Depuración de currencies relacionados con Países
                $currencies_row = $this->db->query("SELECT * FROM currencies WHERE allus_le_pais_id IN ('" . $paises_id . "')");
                $currencies = [];
                $currencies_id = [];

                if ($currencies_row->num_rows > 0) {
                    while ($row = $currencies_row->fetch_assoc()) {
                        $currencies[] = $row;
                    }

                    foreach ($currencies as $key => $object) {
                        $currencies_id[] = $object['id'];
                        $this->createInsert($object, 'currencies');
                    }
                    $currencies_id = implode("', '", $currencies_id);
                    $this->deleteLogGeneral($currencies_id, 'currencies', 'currency_id');
                }

                if(empty($currencies_id) == false){
                    //Depuración de currencies_logs relacionados con Países
                    $currencies_logs_row = $this->db->query("SELECT * FROM currencies_logs WHERE currency_id IN ('" . $currencies_id . "')");
                    $currencies_logs = [];
                    $currencies_logs_id = [];

                    if ($currencies_logs_row->num_rows > 0) {
                        while ($row = $currencies_logs_row->fetch_assoc()) {
                            $currencies_logs[] = $row;
                        }

                        foreach ($currencies_logs as $key => $object) {
                            $currencies_logs_id[] = $object['id'];
                            $this->createInsert($object, 'currencies_logs');
                        }
                    }

                    //Depuración de campaigns relacionados con Países
                    $campaigns_row = $this->db->query("SELECT * FROM campaigns WHERE currency_id IN ('" . $currencies_id . "')");
                    $campaigns = [];
                    $campaigns_id = [];

                    if ($campaigns_row->num_rows > 0) {
                        while ($row = $campaigns_row->fetch_assoc()) {
                            $campaigns[] = $row;
                        }

                        foreach ($campaigns as $key => $object) {
                            $campaigns_id[] = $object['id'];
                            $this->createInsert($object, 'campaigns');
                        }

                        $campaigns_id = implode("', '", $campaigns_id);
                        $this->deleteAuditGeneral($campaigns_id, 'campaigns');
                    }
                }

                //Depuración de allus_le_archivos_to_allus_le_paises_rel relacionados con Países
                $allus_le_archivos_to_allus_le_paises_rel_row = $this->db->query("SELECT * FROM allus_le_archivos_to_allus_le_paises_rel WHERE allus_le_paises_id IN ('" . $paises_id . "')");
                $allus_le_archivos_to_allus_le_paises_rel = [];
                $allus_le_archivos_to_allus_le_paises_rel_id = [];

                if ($allus_le_archivos_to_allus_le_paises_rel_row->num_rows > 0) {
                    while ($row = $allus_le_archivos_to_allus_le_paises_rel_row->fetch_assoc()) {
                        $allus_le_archivos_to_allus_le_paises_rel[] = $row;
                    }

                    foreach ($allus_le_archivos_to_allus_le_paises_rel as $key => $object) {
                        $this->createInsert($object, 'allus_le_archivos_to_allus_le_paises_rel');
                    }
                }

                //Depuración de countries_roles asociadas a las cuentas eliminadas
                $countries_roles_row = $this->db->query("SELECT * FROM countries_roles WHERE country_id IN ('" . $paises_id . "')");
                $countries_roles = [];
                $countries_roles_id = [];

                if ($countries_roles_row->num_rows > 0) {
                    while ($row = $countries_roles_row->fetch_assoc()) {
                        $countries_roles[] = $row;
                    }

                    foreach ($countries_roles as $key => $object) {
                        $countries_roles_id[] = $object['id'];
                        $this->createInsert($object, 'countries_roles');
                    }
                }

                //Depuración de countries_roles asociadas a las cuentas eliminadas
                $allus_le_festivos_row = $this->db->query("SELECT * FROM allus_le_festivos WHERE allus_le_pais_id IN ('" . $paises_id . "')");
                $allus_le_festivos = [];
                $allus_le_festivos_id = [];

                if ($allus_le_festivos_row->num_rows > 0) {
                    while ($row = $allus_le_festivos_row->fetch_assoc()) {
                        $allus_le_festivos[] = $row;
                    }

                    foreach ($allus_le_festivos as $key => $object) {
                        $allus_le_festivos_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_festivos');
                    }

                    $allus_le_festivos_id = implode("', '", $allus_le_festivos_id);
                    $this->deleteAuditGeneral($allus_le_festivos_id, 'allus_le_festivos');
                }

                //Depuración de allus_le_genesis_email asociadas a las cuentas eliminadas
                $allus_le_genesis_email_row = $this->db->query("SELECT * FROM allus_le_genesis_email WHERE geem_pais_name IN ('" . $paises_id . "')");
                $allus_le_genesis_email = [];
                $allus_le_genesis_email_id = [];

                if ($allus_le_genesis_email_row->num_rows > 0) {
                    while ($row = $allus_le_genesis_email_row->fetch_assoc()) {
                        $allus_le_genesis_email[] = $row;
                    }

                    foreach ($allus_le_genesis_email as $key => $object) {
                        $allus_le_genesis_email_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_genesis_email');
                    }
                }

                if(empty($allus_le_genesis_email_id) ==  false){
                    $allus_le_genesis_email_id = implode("', '", $allus_le_genesis_email_id);

                    //Depuración de allus_le_genesis_email_adjuntos asociadas a las cuentas eliminadas
                    $allus_le_genesis_email_adjuntos_row = $this->db->query("SELECT * FROM allus_le_genesis_email_adjuntos WHERE geea_gene_email_name IN ('" . $allus_le_genesis_email_id . "')");
                    $allus_le_genesis_email_adjuntos = [];
                    $allus_le_genesis_email_adjuntos_id = [];

                    if ($allus_le_genesis_email_adjuntos_row->num_rows > 0) {
                        while ($row = $allus_le_genesis_email_adjuntos_row->fetch_assoc()) {
                            $allus_le_genesis_email_adjuntos[] = $row;
                        }

                        foreach ($allus_le_genesis_email_adjuntos as $key => $object) {
                            $allus_le_genesis_email_adjuntos_id[] = $object['id'];
                            $this->createInsert($object, 'allus_le_genesis_email_adjuntos');
                        }
                    }
                }

                //Depuración de allus_le_horarios_asignacion asociadas a las cuentas eliminadas
                $allus_le_horarios_asignacion_row = $this->db->query("SELECT * FROM allus_le_horarios_asignacion WHERE hoas_pais_id IN ('" . $paises_id . "')");
                $allus_le_horarios_asignacion = [];
                $allus_le_horarios_asignacion_id = [];

                if ($allus_le_horarios_asignacion_row->num_rows > 0) {
                    while ($row = $allus_le_horarios_asignacion_row->fetch_assoc()) {
                        $allus_le_horarios_asignacion[] = $row;
                    }

                    foreach ($allus_le_horarios_asignacion as $key => $object) {
                        $allus_le_horarios_asignacion_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_horarios_asignacion');
                    }
                    $allus_le_horarios_asignacion_id = implode("', '", $allus_le_horarios_asignacion_id);
                    $this->deleteAuditGeneral($allus_le_horarios_asignacion_id, 'allus_le_horarios_asignacion');
                }

                //Depuración de allus_le_noticias_to_allus_le_paises_rel asociadas a los Países
                $allus_le_noticias_to_allus_le_paises_rel_row = $this->db->query("SELECT * FROM allus_le_noticias_to_allus_le_paises_rel WHERE allus_le_paises IN ('" . $paises_id . "')");
                $allus_le_noticias_to_allus_le_paises_rel = [];
                $allus_le_noticias_to_allus_le_paises_rel_id = [];

                if ($allus_le_noticias_to_allus_le_paises_rel_row->num_rows > 0) {
                    while ($row = $allus_le_noticias_to_allus_le_paises_rel_row->fetch_assoc()) {
                        $allus_le_noticias_to_allus_le_paises_rel[] = $row;
                    }

                    foreach ($allus_le_noticias_to_allus_le_paises_rel as $key => $object) {
                        $allus_le_noticias_to_allus_le_paises_rel_id[] = $object['allus_le_noticias_id'];
                        $this->createInsert($object, 'allus_le_noticias_to_allus_le_paises_rel');
                    }
                }

                //Depuración de allus_le_paises_emailaccounts asociadas a los Países
                $allus_le_paises_emailaccounts_row = $this->db->query("SELECT * FROM allus_le_paises_emailaccounts WHERE pacc_pais_id IN ('" . $paises_id . "')");
                $allus_le_paises_emailaccounts = [];
                $allus_le_paises_emailaccounts_id = [];

                if ($allus_le_paises_emailaccounts_row->num_rows > 0) {
                    while ($row = $allus_le_paises_emailaccounts_row->fetch_assoc()) {
                        $allus_le_paises_emailaccounts[] = $row;
                    }

                    foreach ($allus_le_paises_emailaccounts as $key => $object) {
                        $allus_le_paises_emailaccounts_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_paises_emailaccounts');
                    }
                    $allus_le_paises_emailaccounts_id = implode("', '", $allus_le_paises_emailaccounts_id);
                    $this->deleteAuditGeneral($allus_le_paises_emailaccounts_id, 'allus_le_paises_emailaccounts');
                }

                //Depuración de allus_le_paises_roles asociadas a los Países
                $allus_le_paises_roles_row = $this->db->query("SELECT * FROM allus_le_paises_roles WHERE paro_pais_id IN ('" . $paises_id . "')");
                $allus_le_paises_roles = [];
                $allus_le_paises_roles_id = [];

                if ($allus_le_paises_roles_row->num_rows > 0) {
                    while ($row = $allus_le_paises_roles_row->fetch_assoc()) {
                        $allus_le_paises_roles[] = $row;
                    }

                    foreach ($allus_le_paises_roles as $key => $object) {
                        $allus_le_paises_roles_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_paises_roles');
                    }

                    $allus_le_paises_roles_id = implode("', '", $allus_le_paises_roles_id);
                    $this->deleteAuditGeneral($allus_le_paises_roles_id, 'allus_le_paises_roles');
                }

                //Depuración de allus_le_vendedores asociadas a los Países
                $allus_le_vendedores_row = $this->db->query("SELECT * FROM allus_le_vendedores WHERE vend_pais_id IN ('" . $paises_id . "')");
                $allus_le_vendedores = [];
                $allus_le_vendedores_id = [];

                if ($allus_le_vendedores_row->num_rows > 0) {
                    while ($row = $allus_le_vendedores_row->fetch_assoc()) {
                        $allus_le_vendedores[] = $row;
                    }

                    foreach ($allus_le_vendedores as $key => $object) {
                        $allus_le_vendedores_id[] = $object['id'];
                        $this->createInsert($object, 'allus_le_vendedores');
                    }

                    $allus_le_vendedores_id = implode("', '", $allus_le_vendedores_id);
                    $this->deleteAuditGeneral($allus_le_vendedores_id, 'allus_le_vendedores');
                }

                //Depuración de doa_flow asociadas a los Países
                $doa_flow_row = $this->db->query("SELECT * FROM doa_flow WHERE country_id IN ('" . $paises_id . "')");
                $doa_flow = [];
                $doa_flow_id = [];

                if ($doa_flow_row->num_rows > 0) {
                    while ($row = $doa_flow_row->fetch_assoc()) {
                        $doa_flow[] = $row;
                    }

                    foreach ($doa_flow as $key => $object) {
                        $doa_flow_id[] = $object['id'];
                        $this->createInsert($object, 'doa_flow');
                    }
                }

                if(empty($doa_flow_id) == false){

                    $doa_flow_id = implode("', '", $doa_flow_id);
                    //Depuración de doa_flow_ranges asociadas a los Países
                    $doa_flow_ranges_row = $this->db->query("SELECT * FROM doa_flow_ranges WHERE doa_flow_id IN ('" . $doa_flow_id . "')");
                    $doa_flow_ranges = [];
                    $doa_flow_ranges_id = [];

                    if ($doa_flow_ranges_row->num_rows > 0) {
                        while ($row = $doa_flow_ranges_row->fetch_assoc()) {
                            $doa_flow_ranges[] = $row;
                        }

                        foreach ($doa_flow_ranges as $key => $object) {
                            $doa_flow_ranges_id[] = $object['id'];
                            $this->createInsert($object, 'doa_flow_ranges');
                        }
                    }

                    if(empty($doa_flow_ranges_id) == false){
                        $doa_flow_ranges_id = implode("', '", $doa_flow_ranges_id);
                        //Depuración de doa_flow_levels_ranges asociadas a los Países
                        $doa_flow_levels_ranges_row = $this->db->query("SELECT * FROM doa_flow_levels_ranges WHERE doa_flow_range_id IN ('" . $doa_flow_ranges_id . "')");
                        $doa_flow_levels_ranges = [];
                        $doa_flow_levels_ranges_id = [];

                        if ($doa_flow_levels_ranges_row->num_rows > 0) {
                            while ($row = $doa_flow_levels_ranges_row->fetch_assoc()) {
                                $doa_flow_levels_ranges[] = $row;
                            }

                            foreach ($doa_flow_levels_ranges as $key => $object) {
                                $doa_flow_levels_ranges_id[] = $object['id'];
                                $this->createInsert($object, 'doa_flow_levels_ranges');
                            }
                        }
                    }
                }

                //Depuración de doa_flow asociadas a los Países
                $genesis_email_row = $this->db->query("SELECT * FROM genesis_email WHERE geem_pais_name IN ('" . $paises_id . "')");
                $genesis_email = [];
                $genesis_email_id = [];

                if ($genesis_email_row->num_rows > 0) {
                    while ($row = $genesis_email_row->fetch_assoc()) {
                        $genesis_email[] = $row;
                    }

                    foreach ($genesis_email as $key => $object) {
                        $genesis_email_id[] = $object['id'];
                        $this->createInsert($object, 'genesis_email');
                    }

                    $genesis_email_id = implode("', '", $genesis_email_id);
                }

                //Depuración de hiq_specifications asociadas a los Países
                $hiq_specifications_row = $this->db->query("SELECT * FROM hiq_specifications WHERE country_id IN ('" . $paises_id . "')");
                $hiq_specifications = [];
                $hiq_specifications_id = [];

                if ($hiq_specifications_row->num_rows > 0) {
                    while ($row = $hiq_specifications_row->fetch_assoc()) {
                        $hiq_specifications[] = $row;
                    }

                    foreach ($hiq_specifications as $key => $object) {
                        $hiq_specifications_id[] = $object['id'];
                        $this->createInsert($object, 'hiq_specifications');
                    }
                }

                //Depuración de hiq_cylinders asociadas a los Países
                $hiq_cylinders_row = $this->db->query("SELECT * FROM hiq_cylinders WHERE country_id IN ('" . $paises_id . "')");
                $hiq_cylinders = [];
                $hiq_cylinders_id = [];

                if ($hiq_cylinders_row->num_rows > 0) {
                    while ($row = $hiq_cylinders_row->fetch_assoc()) {
                        $hiq_cylinders[] = $row;
                    }

                    foreach ($hiq_cylinders as $key => $object) {
                        $hiq_cylinders_id[] = $object['id'];
                        $this->createInsert($object, 'hiq_cylinders');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $oi_config_amounts_row = $this->db->query("SELECT * FROM oi_config_amounts WHERE country_id IN ('" . $paises_id . "')");
                $oi_config_amounts = [];
                $oi_config_amounts_id = [];

                if ($oi_config_amounts_row->num_rows > 0) {
                    while ($row = $oi_config_amounts_row->fetch_assoc()) {
                        $oi_config_amounts[] = $row;
                    }

                    foreach ($oi_config_amounts as $key => $object) {
                        $oi_config_amounts_id[] = $object['id'];
                        $this->createInsert($object, 'oi_config_amounts');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $oi_config_branchoffice_row = $this->db->query("SELECT * FROM oi_config_branchoffice WHERE country_id IN ('" . $paises_id . "')");
                $oi_config_branchoffice = [];
                $oi_config_branchoffice_id = [];

                if ($oi_config_branchoffice_row->num_rows > 0) {
                    while ($row = $oi_config_branchoffice_row->fetch_assoc()) {
                        $oi_config_branchoffice[] = $row;
                    }

                    foreach ($oi_config_branchoffice as $key => $object) {
                        $oi_config_branchoffice_id[] = $object['id'];
                        $this->createInsert($object, 'oi_config_amounts');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $tc_dropdowns_country_row = $this->db->query("SELECT * FROM tc_dropdowns_country WHERE id_country IN ('" . $paises_id . "')");
                $tc_dropdowns_country = [];
                $tc_dropdowns_country_id = [];

                if ($tc_dropdowns_country_row->num_rows > 0) {
                    while ($row = $tc_dropdowns_country_row->fetch_assoc()) {
                        $tc_dropdowns_country[] = $row;
                    }

                    foreach ($tc_dropdowns_country as $key => $object) {
                        $tc_dropdowns_country_id[] = $object['id'];
                        $this->createInsert($object, 'tc_dropdowns_country');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $users_allus_le_paises_row = $this->db->query("SELECT * FROM users_allus_le_paises WHERE allus_le_paises_id IN ('" . $paises_id . "')");
                $users_allus_le_paises = [];
                $users_allus_le_paises_id = [];

                if ($users_allus_le_paises_row->num_rows > 0) {
                    while ($row = $users_allus_le_paises_row->fetch_assoc()) {
                        $users_allus_le_paises[] = $row;
                    }

                    foreach ($users_allus_le_paises as $key => $object) {
                        $users_allus_le_paises_id[] = $object['id'];
                        $this->createInsert($object, 'users_allus_le_paises');
                    }
                }

                //Depuración de allus_le_paises
                foreach ($countries as $key => $object) {
                    $this->createInsert($object, 'allus_le_paises');
                }
            }
            $this->copyTableFull();
            $this->db->close();
            header("Location: ./index.php?message=OK");
        }else{
            $message = "El o los Países seleccionado en el criterio de Depuración no tiene datos almacenados.";
            header('Location: index.php?error='.$message);
        }
    }

    /**
     * @param $object
     * @param $table
     * @etrurn bool
     * Función para eliminar registros de una tabla y escribir el archivo con el Insert y toda la información
     */
    public function createInsert($object, $table)
    {
        if (empty($object) == false && empty($table) == false) {
            $insert = 'INSERT IGNORE INTO ' . $table;
            $names = '';
            $values = '';

            if (empty($object['id']) == false) {
                //$this->db->query('DELETE FROM '.$table.' WHERE id LIKE \''.$object['id'].'\'');
            }

            foreach ($object as $key => $value) {
                if (empty($names) == false) {
                    $names .= ',';
                }
                $names .= $key;

                if (empty($values) == false) {
                    $values .= ',';
                }
                $values .= '\'' . $value . '\'';
            }

            if (empty($names) == false && empty($values) == false) {
                $insert .= '(' . $names . ') VALUES (' . $values . ');';
                $file = fopen("insert.txt", "a+");
                fwrite($file, $insert . PHP_EOL);
                fclose($file);
            }
        }
        return true;
    }

    /**
     * @param $object
     * @param $table
     * @return bool
     * Función para eliminar registros de una tabla y escribir el archivo con el Insert y toda la información
     */
    public function createInsertUsers($object, $table)
    {
        if (empty($object) == false && empty($table) == false) {
            $insert = 'INSERT IGNORE INTO ' . $table;
            $names = '';
            $values = '';

            foreach ($object as $key => $value) {
                if (empty($names) == false) {
                    $names .= ',';
                }
                $names .= $key;

                if (empty($values) == false) {
                    $values .= ',';
                }
                $values .= '\'' . $value . '\'';
            }

            if (empty($names) == false && empty($values) == false) {
                $insert .= '(' . $names . ') VALUES (' . $values . ');';
                $file = fopen("insert.txt", "a+");
                fwrite($file, $insert . PHP_EOL);
                fclose($file);
            }
        }
        return true;
    }



    /**
     * @param $objects_id
     * @param $table
     * @param $column_parent
     * @return bool
     * Función recursiva para eliminar los registros Padres
     */
    public function deleteParentsGeneral($objects_id, $table, $column_parent)
    {
        if (empty($objects_id) == false && empty($table) == false && empty($column_parent) == false) {
            $objects_row = $this->db->query("SELECT * FROM " . $table . " WHERE id IN ('" . $objects_id . "')");
            $objects = [];
            $objects_parents = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    if (empty($object[$column_parent]) == false) {
                        $objects_parents[] = $object[$column_parent];
                    }
                    $this->createInsert($object, $table);
                }

                if (empty($objects_parents) == false) {
                    $objects_parents = implode("', '", $objects_parents);
                    $this->deleteParentsGeneral($objects_parents, $table, $column_parent);
                } else {
                    return false;
                }
            }
        }
    }

    public function deleteAuditGeneral($objects_id, $table)
    {
        if (empty($objects_id) == false && empty($table) == false) {
            $objects_row = $this->db->query("SELECT * FROM " . $table . "_audit WHERE parent_id IN ('" . $objects_id . "')");
            $objects = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    $this->createInsert($object, $table.'_audit');
                }
            }
        }
    }

    public function deleteLogGeneral($objects_id, $table, $column)
    {
        if (empty($objects_id) == false && empty($table) == false && empty($column) == false) {
            $objects_row = $this->db->query("SELECT * FROM " . $table . "_logs WHERE ".$column." IN ('" . $objects_id . "')");
            $objects = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    $this->createInsert($object, $table);
                }
            }
        }
    }


    public function deleteBugGeneral($objects_id, $table, $column)
    {
        if (empty($objects_id) == false && empty($table) == false && empty($column) == false) {

            $objects_row = $this->db->query("SELECT * FROM " . $table . "_bug WHERE " . $column . " IN ('" . $objects_id . "')");
            $objects = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    $this->createInsert($object, $table);
                }
            }
        }
    }

    public function copyTableFull()
    {
        $tables = [
            'accounts',
            'accounts_cases',
            'accounts_contacts',
            'accounts_opportunities',
            'acl_actions',
            'acl_applications',
            'acl_relationship_applications',
            'acl_roles',
            'acl_roles_users',
            'acl_roles_actions',
            'activity_sub_activities',
            'allus_admin',
            'allus_admin_audit',
            'allus_arbol',
            'allus_arbol_audit',
            'allus_arbol_camposadicionales_asociados',
            'allus_arbol_camposadicionales_asociados_audit',
            'allus_arbol_roles',
            'allus_arbol_roles_audit',
            'allus_le_archivos',
            'allus_le_archivos_roles',
            'allus_le_archivos_to_aclroles_rel',
            'allus_archivos_adjuntos',
            'allus_archivos_adjuntos_audit',
            'allus_archivos_roles',
            'allus_archivos_roles_audit',
            'allus_sc_mainui',
            'allus_uh_moviles_to_allus_uh_horarios_rel',
            'allus_uh_mutualistas_to_allus_uh_equipos_rel',
            'allus_horario_laboral',
            'allus_horario_laboral_rangos',
            'allus_le_campanabulk',
            'allus_le_campanabulk_audit',
            'allus_le_campanabulk_registros',
            'allus_le_campanabulk_registros_audit',
            'allus_le_clientesbulk_contactos_xref',
            'allus_le_clientesbulk_conversiones',
            'allus_le_clientesbulk_conversiones_audit',
            'allus_le_clientesbulk_tanques',
            'allus_le_clientesbulk_tanques_audit',
            'allus_le_clientesbulk_emails',
            'allus_le_clientesbulk_emails_audit',
            'allus_le_campanabulk_registros_reportes_niveles',
            'allus_le_campanabulk_registros_reportes_niveles_tanques',
            'allus_le_cargasbulk_calllist',
            'allus_le_cargasbulk_calllist_audit',
            'allus_le_cargasbulk_calllist_registros',
            'allus_le_cargasbulk_calllist_registros_audit',
            'allus_le_cargasbulk_clientes',
            'allus_le_cargasbulk_clientes_audit',
            'allus_le_cargasbulk_clientes_registros',
            'allus_le_cargasbulk_clientes_registros_audit',
            'allus_le_cargasbulk_tanques',
            'allus_le_cargasbulk_tanques_audit',
            'allus_le_cargasbulk_tanques_registros',
            'allus_le_cargasbulk_tanques_registros_audit',
            'allus_le_cargasclientes',
            'allus_le_cargasclientes_registros',
            'allus_le_clientesbulk',
            'allus_le_clientesbulk_audit',
            'allus_le_clientesbulk_contactos',
            'allus_le_clientesbulk_contactos_audit',
            'allus_le_clientesbulk_homologaciones',
            'allus_le_clientesbulk_homologaciones_audit',
            'allus_le_clientesbulk_tanques_audit',
            'allus_le_clientesbulk_telefonos',
            'allus_le_productos',
            'allus_le_productos_audit',
            'allus_library',
            'allus_library_audit',
            'allus_le_noticias',
            'allus_le_noticias_roles',
            'allus_le_noticias_to_aclroles_rel',
            'allus_le_archivos_cerificados',
            'allus_le_archivos_cerificados_audit',
            'allus_em_email_frequent_addresses',
            'allus_relacion_3dl',
            'allus_relacion_3dl_audit',
            'allus_reports',
            'allus_rm_level',
            'allus_ruta_estados',
            'allus_ruta_estados_audit',
            'allus_ruta_estados_detalles',
            'allus_ruta_estados_detalles_audit',
            'allus_sc_assets',
            'allus_sc_assets_audit',
            'allus_sc_availabilities',
            'allus_sc_availabilities_audit',
            'allus_si',
            'allus_si_audit',
            'allus_workflow',
            'allus_workflow_actions',
            'allus_workflow_actions_audit',
            'allus_workflow_audit',
            'allus_workflow_conditions',
            'allus_workflow_conditions_audit',
            'allus_workflow_log',
            'allus_workflow_log_audit',
            'address_book',
            'bugs',
            'bugs_audit',
            'calls',
            'cases',
            'cases_audit',
            'cases_bugs',
            'calls_contacts',
            'calls_leads',
            'call_scheduling',
            'cases_notes',
            'calls_users',
            'campaign_trkrs',
            'campaign_log',
            'causes',
            'config',
            'contacts',
            'contacts_bugs',
            'contacts_audit',
            'contacts_cases',
            'cron_remove_documents',
            'custom_fields',
            'documents',
            'documents_cases',
            'document_revisions',
            'documents_accounts',
            'documents_bugs',
            'documents_contacts',
            'documents_opportunities',
            'eapm',
            'emails',
            'emails_email_addr_rel',
            'emails_beans',
            'email_addresses',
            'emailman',
            'email_marketing',
            'email_marketing_prospect_lists',
            'email_cache',
            'emails_text',
            'email_addr_bean_rel',
            'fields_meta_data',
            'folders',
            'folders_rel',
            'folders_subscriptions',
            'hiq_components',
            'hiq_status',
            'hiq_import',
            'hiq_import_log',
            'igma_logs',
            'igma_logs_audit',
            'import_maps',
            'import_clientes_bulk_contactos',
            'import_clientes_bulk_conversiones',
            'import_clientes_bulk_email_preferred',
            'import_clientes_bulk_emails',
            'import_vendedores',
            'inbound_email',
            'inbound_email_autoreply',
            'leads',
            'leads_audit',
            'lems_states',
            'job_queue',
            'leads_audit',
            'linked_documents',
            'meetings',
            'meetings_contacts',
            'meetings_users',
            'meetings_leads',
            'notes',
            'inbound_email_cache_ts',
            'io_sequential',
            'oauth_nonce',
            'oauth_consumer',
            'oauth_tokens',
            'migrations',
            'oauth_nonce',
            'opportunities',
            'opportunities_audit',
            'opportunities_contacts',
            'outbound_email',
            'permission_role',
            'permissions',
            'password_resets',
            'project',
            'projects_accounts',
            'project_task',
            'project_task_audit',
            'projects_cases',
            'projects_contacts',
            'projects_opportunities',
            'projects_products',
            'prospect_list_campaigns',
            'prospect_lists',
            'prospect_lists_prospects',
            'prospects',
            'releases',
            'relationships',
            'reports_filters',
            'role_applications',
            'reports',
            'role_user',
            'roles',
            'roles_users',
            'roles_modules',
            'sessions',
            'saved_search',
            'schedulers',
            'seeds',
            'sugarfeed',
            'sessions',
            'tito_sign',
            'tasks',
            'tc_answer_option',
            'tc_call_response',
            'tc_dropdown',
            'tc_field_dropdown',
            'tc_inquiry',
            'tc_inquiry_questions',
            'tc_inquiry_response',
            'tc_log_execution_st',
            'tc_olsons',
            'tc_relation_dropdowns',
            'tc_olfather',
            'ts_manpower',
            'ts_materials',
            'tracker',
            'upgrade_history',
            'user_preferences',
            'users_applications',
            'users_feeds',
            'users_last_import',
            'users_signatures',
            'users_password_link',
            'versions',
            'vcals'
        ];

        foreach ($tables as $table){
            if(empty($table) == false){
                $sql_row = $this->db->query("SELECT * FROM ".$table);
                $sql = [];

                if ($sql_row->num_rows > 0) {
                    while ($row = $sql_row->fetch_assoc()) {
                        $sql[] = $row;
                    }

                    foreach ($sql as $key => $object) {
                        $insert = 'INSERT IGNORE INTO ' . $table;
                        $names = '';
                        $values = '';

                        foreach ($object as $key => $value) {
                            if (empty($names) == false) {
                                $names .= ',';
                            }
                            $names .= $key;

                            if (empty($values) == false) {
                                $values .= ',';
                            }
                            $values .= '\'' . $value . '\'';
                        }

                        if (empty($names) == false && empty($values) == false) {
                            $insert .= '(' . $names . ') VALUES (' . $values . ');';
                            $file = fopen("insert.txt", "a+");
                            fwrite($file, $insert . PHP_EOL);
                            fclose($file);
                        }
                    }
                }
            }
        }
    }
}