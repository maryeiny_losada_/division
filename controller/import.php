<?php

/*
 * Controllador para realizar la Importación
 */

class ImportController
{

    private $service;
    private $nameDB;
    private $db;
    private $fileinsert = null;
    private $init;
    private $fileName;

    public function __construct($nameDB, $fileinsert, $init, $fileName = null)
    {
        $_GET['message'] = '';
        $this->nameDB = $nameDB;

        if(empty($fileinsert) == false){
            $this->fileinsert = $fileinsert;
        }

        if(empty($fileName) == false){
            $this->fileName = $fileName;
        }
        $this->init = $init;
        $this->service = array();
        $mysqli = mysqli_connect("localhost", "root", "", $nameDB);

        if (!$mysqli) {
            $message = "No se pudo conectar a MySQL. Error: ".mysqli_connect_errno();
            header('Location: import.php?error='.$message);
            exit;
        }
        $this->db = $mysqli;
    }

    public function import()
    {
        if(empty($this->fileinsert) == false){
            if (strcmp($this->fileinsert['type'], 'text/plain') !== 0) {
                $message = 'Solo se aceptan archivos planos para la importanción.';
                header('Location: import.php?error=' . $message);
                exit;
            }
            $this->fileName = $this->upload($this->fileinsert);
        }

        if($this->fileName != false){
            $countFile = count(file($this->fileName));
            $file = fopen($this->fileName, "r");
            $numInit = $this->init;
            $numLimit = $this->init + 1000;
            $numLinea = 0;

            while ($linea = fgets($file)) {
                if($numLinea >= $numInit && $numLinea < $numLimit){
                    $this->db->query($linea);
                }
                $numLinea++;
                if($numLinea == $numLimit){
                    $_SESSION['nameDB'] = $this->nameDB;
                    $_SESSION['fileName'] = $this->fileName;
                    $_SESSION['init'] = $numLinea;
                    fclose($file);

                    $file = fopen("full_insert.txt", "a+");
                    fwrite($file, 'Nro. de Líneas analizadas: '.$numLinea .'. Fecha: '.date("Y-m-d H:i:s"). PHP_EOL);
                    fclose($file);
                    header('Refresh:2; url=route.php');
                    exit;
                }else if($countFile == $numLinea){
                    $file = fopen("full_insert.txt", "a+");
                    fwrite($file, 'Nro. de Líneas analizadas: '.$numLinea .'. Fecha: '.date("Y-m-d H:i:s"). PHP_EOL);
                    fclose($file);
                    session_destroy();
                    header("Location: ./import.php?message=OK");
                    exit;
                }
            }

            fclose($file);
            session_destroy();
            header("Location: ./import.php?message=OK");
            exit;
        }

        header("Location: ./import.php");
        exit;
    }

    public function upload($file){
        if(empty($file) == false){
            if (move_uploaded_file($file['tmp_name'], './insert_temp.txt')) {
                return 'insert_temp.txt';
            } else {
                return false;
            }
        }
    }
}
