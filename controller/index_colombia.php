<?php

/*
 * Controllador para realizar la Conexión
 */

class IndexColombiaController
{
    private $service;
    private $nameDB;
    private $db;
    private $countries;
    private $country;

    public function __construct()
    {
        $this->nameDB = 'crm_linde_colombia';
        $this->country = 'latam';
        $this->service = array();
        $mysqli = mysqli_connect("localhost", "root", "", $this->nameDB);
        //$mysqli = mysqli_connect("172.102.100.121", "linde_test", "Linde2018*", $nameDB);

        if (!$mysqli) {
            $message = "No se pudo conectar a MySQL. Error: " . mysqli_connect_errno();
            echo 'Error: ' . $message;
        }
        $this->db = $mysqli;
    }

    public function depuration()
    {
        if (strcmp($this->country, 'latam') === 0) {
            $like = 'NOT LIKE';
        } else {
            $like = 'LIKE';
        }
        $like_full = " p.name " . $like . " 'colombia' ";
        $this->countries = $this->db->query("SELECT * FROM allus_le_paises WHERE name " . $like . " 'colombia'");

        if ($this->countries->num_rows > 0) {

            while ($row = $this->countries->fetch_assoc()) {
                $paises_id[] = $row['id'];
                $countries[] = $row;
            }

            do{
                //Depuración de users
                $user_row = $this->db->query("SELECT u.id FROM users u INNER JOIN allus_le_paises p ON p.id = u.user_pais_id WHERE " . $like_full . " AND u.user_name != 'admin.allus' LIMIT 100");
                $user_cont = $user_row->num_rows;
                if ($user_cont > 0) {
                    while ($row = $user_row->fetch_assoc()) {
                        $this->deleteRow($row['id'], 'users');
                        unset($row);
                    }
                }
                unset($user_row);
            }while($user_cont > 0);

            do{
                //Depuración de allus_le_paises_users_rel asociadas a los Países
                $allus_le_paises_users_rel_row = $this->db->query("SELECT a.id FROM allus_le_paises_users_rel a INNER JOIN allus_le_paises p ON p.id = a.allus_le_paises_id WHERE " . $like_full." LIMIT 100");
                $allus_le_paises_users_rel_cont = $allus_le_paises_users_rel_row->num_rows;
                if ($allus_le_paises_users_rel_cont > 0) {
                    while ($row = $allus_le_paises_users_rel_row->fetch_assoc()) {
                        $this->deleteRow($row['id'], 'allus_le_paises_users_rel');
                        unset($row);
                    }
                }
                unset($allus_le_paises_users_rel_row);
            }while($allus_le_paises_users_rel_cont > 0);

            if (empty($paises_id) == false) {
                //Depuración de allus_em_accounts por País eliminados
                $allus_em_accounts_row = $this->db->query("SELECT c.id FROM allus_em_accounts c INNER JOIN allus_le_paises p ON p.id = c.pais_id WHERE " . $like_full);
                $allus_em_accounts_id = [];

                if ($allus_em_accounts_row->num_rows > 0) {
                    while ($row = $allus_em_accounts_row->fetch_assoc()) {
                        $allus_em_accounts_id[] = $row['id'];
                        echo 'Depurado tabla: allus_em_accounts' . '\n';
                        $this->createInsert($row, 'accounts');
                    }
                }

                if (empty($allus_em_accounts_id) == false) {
                    //Depuración de allus_em_emails asociadas a las cuentas eliminadas

                    $emails_row = $this->db->query("SELECT e.id FROM allus_em_emails e INNER JOIN allus_em_accounts em ON em.id = e.em_accounts_id INNER JOIN allus_le_paises p ON p.id = em.pais_id  WHERE " . $like_full);
                    $emails_id = [];

                    if ($emails_row->num_rows > 0) {
                        while ($row = $emails_row->fetch_assoc()) {
                            $emails_id[] = $row['id'];
                            echo 'Depurado tabla: allus_em_emails' . '\n';
                            $this->createInsert($row, 'allus_em_emails');
                        }
                    }

                    if (empty($emails_id) == false) {
                        //Depuración de allus_em_emails_cases_rel asociadas a las cuentas eliminadas
                        $allus_em_emails_cases_rel_row = $this->db->query("SELECT emr.id FROM allus_em_emails_cases_rel emr INNER JOIN allus_em_emails em ON em.id = emr.em_emails_id INNER JOIN allus_em_accounts ec ON ec.id = em.em_accounts_id INNER JOIN allus_le_paises p ON p.id = ec.pais_id WHERE " . $like_full);

                        if ($allus_em_emails_cases_rel_row->num_rows > 0) {
                            while ($row = $allus_em_emails_cases_rel_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_em_emails_cases_rel' . '\n';
                                $this->createInsert($row, 'allus_em_emails_cases_rel');
                            }
                        }

                        $allus_em_emails_grupos_rel_row = $this->db->query("SELECT emr.id FROM allus_em_emails_grupos_rel emr INNER JOIN allus_em_emails em ON em.id = emr.email_id INNER JOIN allus_em_accounts ec ON ec.id = em.em_accounts_id INNER JOIN allus_le_paises p ON p.id = ec.pais_id WHERE " . $like_full);

                        if ($allus_em_emails_grupos_rel_row->num_rows > 0) {
                            while ($row = $allus_em_emails_grupos_rel_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_em_emails_grupos_rel' . '\n';
                                $this->createInsert($row, 'allus_em_emails_grupos_rel');
                            }
                        }

                        $allus_em_emails_logs_row = $this->db->query("SELECT emr.id FROM allus_em_emails_logs emr INNER JOIN allus_em_emails em ON em.id = emr.em_emails_id INNER JOIN allus_em_accounts ec ON ec.id = em.em_accounts_id INNER JOIN allus_le_paises p ON p.id = ec.pais_id WHERE " . $like_full);

                        if ($allus_em_emails_logs_row->num_rows > 0) {
                            while ($row = $allus_em_emails_logs_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_em_emails_logs' . '\n';
                                $this->createInsert($row, 'allus_em_emails_logs');
                            }
                        }

                        $allus_em_emails_to_allus_em_labels_xref_row = $this->db->query("SELECT emr.id FROM allus_em_emails_to_allus_em_labels_xref emr INNER JOIN allus_em_emails em ON em.id = emr.allus_em_emails_id INNER JOIN allus_em_accounts ec ON ec.id = em.em_accounts_id INNER JOIN allus_le_paises p ON p.id = ec.pais_id WHERE " . $like_full);

                        if ($allus_em_emails_to_allus_em_labels_xref_row->num_rows > 0) {
                            while ($row = $allus_em_emails_to_allus_em_labels_xref_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_em_emails_to_allus_em_labels_xref' . '\n';
                                $this->createInsert($row, 'allus_em_emails_to_allus_em_labels_xref');
                            }
                        }
                    }

                    $allus_em_labels_row = $this->db->query("SELECT e.id FROM allus_em_labels e INNER JOIN allus_em_accounts em ON em.id = e.em_accounts_id INNER JOIN allus_le_paises p ON p.id = em.pais_id  WHERE " . $like_full);

                    if ($allus_em_labels_row->num_rows > 0) {
                        while ($row = $allus_em_labels_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_em_labels' . '\n';
                            $this->createInsert($row, 'allus_em_labels');
                        }
                    }

                    //Depuración de allus_em_mailboxes asociadas a las cuentas eliminadas
                    $allus_em_mailboxes_row = $this->db->query("SELECT e.id FROM allus_em_mailboxes e INNER JOIN allus_em_accounts em ON em.id = e.em_accounts_id INNER JOIN allus_le_paises p ON p.id = em.pais_id  WHERE " . $like_full);

                    if ($allus_em_mailboxes_row->num_rows > 0) {
                        while ($row = $allus_em_mailboxes_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_em_mailboxes' . '\n';
                            $this->createInsert($row, 'allus_em_mailboxes');
                        }
                    }

                    //Depuración de allus_em_accounts_to_allus_grupos_xref asociadas a las cuentas eliminadas
                    $allus_em_accounts_to_allus_grupos_xref_row = $this->db->query("SELECT e.id FROM allus_em_accounts_to_allus_grupos_xref e INNER JOIN allus_em_accounts em ON em.id = e.allus_em_accounts_id INNER JOIN allus_le_paises p ON p.id = em.pais_id  WHERE " . $like_full);

                    if ($allus_em_accounts_to_allus_grupos_xref_row->num_rows > 0) {
                        while ($row = $allus_em_accounts_to_allus_grupos_xref_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_em_accounts_to_allus_grupos_xref' . '\n';
                            $this->createInsert($row, 'allus_em_accounts_to_allus_grupos_xref');
                        }
                    }
                }

                $allus_business_days_row = $this->db->query("SELECT c.id FROM allus_business_days c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($allus_business_days_row->num_rows > 0) {
                    while ($row = $allus_business_days_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_business_days' . '\n';
                        $this->createInsert($row, 'allus_business_days');
                    }
                }
                //Depuración de allus_clientes relacionas con Paises
                $allus_clientes_row = $this->db->query("SELECT c.id FROM allus_clientes c INNER JOIN allus_le_paises p ON p.id = c.clie_pais_id WHERE " . $like_full);
                $allus_clientes_id = [];

                if ($allus_clientes_row->num_rows > 0) {
                    while ($row = $allus_clientes_row->fetch_assoc()) {
                        $allus_clientes_id = $row['id'];
                        echo 'Depurado tabla: allus_clientes' . '\n';
                        $this->createInsert($row, 'allus_clientes');
                    }
                }

                if (empty($allus_clientes_id) == false) {
                    //Depuración de allus_contactos relacionados con Clientes
                    $allus_contactos_row = $this->db->query("SELECT c.id FROM allus_contactos c INNER JOIN allus_clientes cm ON cm.id = c.cont_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                    if ($allus_contactos_row->num_rows > 0) {
                        while ($row = $allus_contactos_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_clientes' . '\n';
                            $this->createInsert($row, 'allus_contactos');
                        }
                    }

                    //Depuración de allus_em_emails_allus_clientes_rel relacionados con Clientes
                    $allus_em_emails_allus_clientes_rel_row = $this->db->query("SELECT c.id FROM allus_em_emails_allus_clientes_rel c INNER JOIN allus_clientes cm ON cm.id = c.allus_cliente_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                    if ($allus_em_emails_allus_clientes_rel_row->num_rows > 0) {
                        while ($row = $allus_em_emails_allus_clientes_rel_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_em_emails_allus_clientes_rel' . '\n';
                            $this->createInsert($row, 'allus_em_emails_allus_clientes_rel');
                        }
                    }

                    //Depuración de allus_interacciones relacionados con Clientes
                    $allus_interacciones_row = $this->db->query("SELECT c.id FROM allus_interacciones c INNER JOIN allus_clientes cm ON cm.id = c.inte_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                    if ($allus_interacciones_row->num_rows > 0) {
                        while ($row = $allus_interacciones_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_interacciones' . '\n';
                            $this->createInsert($row, 'allus_interacciones');
                        }
                    }

                    //Depuración de allus_notas_credito_debito relacionados con Clientes
                    $allus_notas_credito_debito_row = $this->db->query("SELECT c.id FROM allus_notas_credito_debito c INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $allus_notas_credito_debito_id = [];

                    if ($allus_notas_credito_debito_row->num_rows > 0) {
                        while ($row = $allus_notas_credito_debito_row->fetch_assoc()) {
                            $allus_notas_credito_debito_id[] = $row['id'];
                            echo 'Depurado tabla: allus_notas_credito_debito' . '\n';
                            $this->createInsert($row, 'allus_notas_credito_debito');
                        }
                    }

                    if (empty($allus_notas_credito_debito_id) == false) {
                        //Depuración de allus_nc_adjuntos relacionados con NC
                        $allus_nc_adjuntos_row = $this->db->query("SELECT na.id FROM allus_nc_adjuntos na INNER JOIN allus_notas_credito_debito c ON c.id = na.allus_nc_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_nc_adjuntos_row->num_rows > 0) {
                            while ($row = $allus_nc_adjuntos_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_nc_adjuntos' . '\n';
                                $this->createInsert($row, 'allus_nc_adjuntos');
                            }
                        }

                        //Depuración de allus_nc_facturas relacionados con NC
                        $allus_nc_facturas_row = $this->db->query("SELECT na.id FROM allus_nc_facturas na INNER JOIN allus_notas_credito_debito c ON c.id = na.allus_nc_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_nc_facturas_row->num_rows > 0) {
                            while ($row = $allus_nc_facturas_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_nc_facturas' . '\n';
                                $this->createInsert($row, 'allus_nc_facturas');
                            }
                        }

                        //Depuración de allus_nc_gestiones relacionados con NC
                        $allus_nc_gestiones_row = $this->db->query("SELECT na.id FROM allus_nc_gestiones na INNER JOIN allus_notas_credito_debito c ON c.id = na.gest_nc_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_nc_gestiones_row->num_rows > 0) {
                            while ($row = $allus_nc_gestiones_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_nc_gestiones' . '\n';
                                $this->createInsert($row, 'allus_nc_gestiones');
                            }
                        }

                        //Depuración de allus_nc_reasignacion relacionados con NC
                        $allus_nc_reasignacion_row = $this->db->query("SELECT na.id FROM allus_nc_reasignacion na INNER JOIN allus_notas_credito_debito c ON c.id = na.nota_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_nc_reasignacion_row->num_rows > 0) {
                            while ($row = $allus_nc_reasignacion_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_nc_reasignacion' . '\n';
                                $this->createInsert($row, 'allus_nc_reasignacion');
                            }
                        }

                        //Depuración de allus_workflow_notifications_log relacionados con NC
                        $allus_workflow_notifications_log_row = $this->db->query("SELECT na.id FROM allus_workflow_notifications_log na INNER JOIN allus_notas_credito_debito c ON c.id = na.nota_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_workflow_notifications_log_row->num_rows > 0) {
                            while ($row = $allus_workflow_notifications_log_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_workflow_notifications_log' . '\n';
                                $this->createInsert($row, 'allus_workflow_notifications_log');
                            }
                        }

                        //Depuración de lems_actions relacionados con NC
                        $lems_actions_row = $this->db->query("SELECT na.id FROM lems_actions na INNER JOIN allus_notas_credito_debito c ON c.id = na.ncnd_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($lems_actions_row->num_rows > 0) {
                            while ($row = $lems_actions_row->fetch_assoc()) {
                                echo 'Depurado tabla: lems_actions' . '\n';
                                $this->createInsert($row, 'lems_actions');
                            }
                        }

                        //Depuración de allus_approval_management relacionados con NC
                        $allus_approval_management_row = $this->db->query("SELECT na.id FROM allus_approval_management na INNER JOIN allus_notas_credito_debito c ON c.id = na.id_notacredito INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_approval_management_row->num_rows > 0) {
                            while ($row = $allus_approval_management_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_approval_management' . '\n';
                                $this->createInsert($row, 'allus_approval_management');
                            }
                        }
                    }

                    //Depuración de allus_rm_cases relacionados con Clientes
                    $allus_rm_cases_row = $this->db->query("SELECT c.id FROM allus_rm_cases c INNER JOIN allus_clientes cm ON cm.id = c.rmcas_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $allus_rm_cases_id = [];

                    if ($allus_rm_cases_row->num_rows > 0) {
                        while ($row = $allus_rm_cases_row->fetch_assoc()) {
                            $allus_rm_cases_id[] = $row['id'];
                            echo 'Depurado tabla: allus_rm_cases' . '\n';
                            $this->createInsert($row, 'allus_rm_cases');
                        }
                    }

                    if (empty($allus_rm_cases_id) == false) {
                        //Depuración de allus_rm_cases relacionados con Clientes
                        $allus_rm_cases_managements_row = $this->db->query("SELECT ca.id FROM allus_rm_cases_managements na INNER JOIN allus_rm_cases c ON c.id = ca.rm_cases_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_rm_cases_managements_row->num_rows > 0) {
                            while ($row = $allus_rm_cases_managements_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_rm_cases_managements' . '\n';
                                $this->createInsert($row, 'allus_rm_cases_managements');
                            }
                        }
                    }

                    //Depuración de allus_sc_appointments relacionados con Clientes
                    $allus_sc_appointments_row = $this->db->query("SELECT c.id FROM allus_sc_appointments c INNER JOIN allus_clientes cm ON cm.id = c.appo_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                    if ($allus_sc_appointments_row->num_rows > 0) {
                        while ($row = $allus_sc_appointments_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_sc_appointments_id' . '\n';
                            $this->createInsert($row, 'allus_sc_appointments_id');
                        }
                    }

                    //Depuración de hiq_requests relacionados con Clientes
                    $hiq_requests_row = $this->db->query("SELECT c.id FROM hiq_requests c INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $hiq_requests_id = [];

                    if ($hiq_requests_row->num_rows > 0) {
                        while ($row = $hiq_requests_row->fetch_assoc()) {
                            $hiq_requests_id[] = $row['id'];
                            echo 'Depurado tabla: hiq_requests' . '\n';
                            $this->createInsert($row, 'hiq_requests');
                        }
                    }

                    if (empty($hiq_requests_id) == false) {
                        $hiq_attachments_row = $this->db->query("SELECT hiq.id FROM hiq_attachments hiq INNER JOIN hiq_requests c ON c.id = hiq.hiq_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($hiq_attachments_row->num_rows > 0) {
                            while ($row = $hiq_attachments_row->fetch_assoc()) {
                                echo 'Depurado tabla: hiq_attachments' . '\n';
                                $this->createInsert($row, 'hiq_attachments');
                            }
                        }

                        //Depuración de hiq_commercial_information relacionados con Clientes
                        $hiq_commercial_information_row = $this->db->query("SELECT hiq.id FROM hiq_commercial_information hiq INNER JOIN hiq_requests c ON c.id = hiq.hiq_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($hiq_commercial_information_row->num_rows > 0) {
                            while ($row = $hiq_commercial_information_row->fetch_assoc()) {
                                echo 'Depurado tabla: hiq_commercial_information' . '\n';
                                $this->createInsert($row, 'hiq_commercial_information');
                            }
                        }

                        //Depuración de hiq_commercial_information relacionados con Clientes
                        $hiq_managements_row = $this->db->query("SELECT hiq.id FROM hiq_managements hiq INNER JOIN hiq_requests c ON c.id = hiq.hiq_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($hiq_managements_row->num_rows > 0) {
                            while ($row = $hiq_managements_row->fetch_assoc()) {
                                echo 'Depurado tabla: hiq_managements' . '\n';
                                $this->createInsert($row, 'hiq_managements');
                            }
                        }

                        //Depuración de hiq_traceability relacionados con Clientes
                        $hiq_traceability_row = $this->db->query("SELECT hiq.id FROM hiq_traceability hiq INNER JOIN hiq_requests c ON c.id = hiq.id_hiq INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($hiq_traceability_row->num_rows > 0) {
                            while ($row = $hiq_traceability_row->fetch_assoc()) {
                                echo 'Depurado tabla: hiq_traceability' . '\n';
                                $this->createInsert($row, 'hiq_traceability');
                            }
                        }

                        //Depuración de hiq_traceability relacionados con Clientes
                        $hiq_workflow_notifications_log_row = $this->db->query("SELECT hiq.id FROM hiq_workflow_notifications_log hiq INNER JOIN hiq_requests c ON c.id = hiq.hiq_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($hiq_workflow_notifications_log_row->num_rows > 0) {
                            while ($row = $hiq_workflow_notifications_log_row->fetch_assoc()) {
                                echo 'Depurado tabla: hiq_workflow_notifications_log' . '\n';
                                $this->createInsert($row, 'hiq_workflow_notifications_log');
                            }
                        }
                    }

                    //Depuración de lems_cases relacionados con Clientes
                    $lems_cases_row = $this->db->query("SELECT c.id FROM lems_cases c INNER JOIN allus_clientes cm ON cm.id = c.client_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $lems_cases_id = [];

                    if ($lems_cases_row->num_rows > 0) {
                        while ($row = $lems_cases_row->fetch_assoc()) {
                            $lems_cases_id[] = $row['id'];
                            echo 'Depurado tabla: lems_cases' . '\n';
                            $this->createInsert($row, 'lems_cases');
                        }
                    }

                    if (empty($lems_cases_id) == false) {
                        //Depuración de lems_cases relacionados con Clientes
                        $lems_actions_row = $this->db->query("SELECT cl.id FROM lems_actions cl INNER JOIN lems_cases c ON c.id = cl.lems_case_id INNER JOIN allus_clientes cm ON cm.id = c.client_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($lems_actions_row->num_rows > 0) {
                            while ($row = $lems_actions_row->fetch_assoc()) {
                                echo 'Depurado tabla: lems_actions' . '\n';
                                $this->createInsert($row, 'lems_actions');
                            }
                        }

                        //Depuración de lems_cases relacionados con Clientes
                        $allus_lems_cases_attachments_row = $this->db->query("SELECT cl.id FROM allus_lems_cases_attachments cl INNER JOIN lems_cases c ON c.id = cl.lems_case_id INNER JOIN allus_clientes cm ON cm.id = c.client_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_lems_cases_attachments_row->num_rows > 0) {
                            while ($row = $allus_lems_cases_attachments_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_lems_cases_attachments' . '\n';
                                $this->createInsert($row, 'allus_lems_cases_attachments');
                            }
                        }

                        $allus_lems_cases_managements_row = $this->db->query("SELECT cl.id FROM allus_lems_cases_managements cl INNER JOIN lems_cases c ON c.id = cl.lems_case_id INNER JOIN allus_clientes cm ON cm.id = c.client_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_lems_cases_managements_row->num_rows > 0) {
                            while ($row = $allus_lems_cases_managements_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_lems_cases_managements' . '\n';
                                $this->createInsert($row, 'allus_lems_cases_managements');
                            }
                        }
                    }

                    //Depuración de io_installation_orders relacionados con Clientes
                    $io_installation_orders_row = $this->db->query("SELECT c.id FROM io_installation_orders c INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $io_installation_orders_id = [];

                    if ($io_installation_orders_row->num_rows > 0) {
                        while ($row = $io_installation_orders_row->fetch_assoc()) {
                            $io_installation_orders_id[] = $row['id'];
                            echo 'Depurado tabla: oi_config_branchoffice' . '\n';
                            $this->createInsert($row, 'oi_config_branchoffice');
                        }
                    }

                    if (empty($io_installation_orders_id) == false) {
                        //Depuración de io_managements relacionados con OI
                        $io_managements_row = $this->db->query("SELECT cl.id FROM io_managements cl INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_managements_row->num_rows > 0) {
                            while ($row = $io_managements_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_managements' . '\n';
                                $this->createInsert($row, 'io_managements');
                            }
                        }

                        //Depuración de io_products relacionados con OI
                        $io_products_row = $this->db->query("SELECT cl.id FROM io_products cl INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_products_row->num_rows > 0) {
                            while ($row = $io_products_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_products' . '\n';
                                $this->createInsert($row, 'io_products');
                            }
                        }

                        //Depuración de io_security relacionados con OI
                        $io_security_row = $this->db->query("SELECT cl.id FROM io_security cl INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_security_row->num_rows > 0) {
                            while ($row = $io_security_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_security' . '\n';
                                $this->createInsert($row, 'io_security');
                            }
                        }

                        //Depuración de io_accounting_report relacionados con OI
                        $io_accounting_report_row = $this->db->query("SELECT cl.id FROM io_accounting_report cl INNER JOIN io_installation_orders c ON c.id = cl.id_order INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_accounting_report_row->num_rows > 0) {
                            while ($row = $io_accounting_report_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_accounting_report' . '\n';
                                $this->createInsert($row, 'io_accounting_report');
                            }
                        }

                        //Depuración de io_attachments relacionados con OI
                        $io_attachments_row = $this->db->query("SELECT cl.id FROM io_attachments cl INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_attachments_row->num_rows > 0) {
                            while ($row = $io_attachments_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_attachments' . '\n';
                                $this->createInsert($row, 'io_attachments');
                            }
                        }

                        //Depuración de io_billing relacionados con OI
                        $io_billing_row = $this->db->query("SELECT cl.id FROM io_billing cl INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_billing_row->num_rows > 0) {
                            while ($row = $io_billing_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_billing' . '\n';
                                $this->createInsert($row, 'io_billing');
                            }
                        }

                        //Depuración de io_business_evaluation relacionados con OI
                        $io_business_evaluation_row = $this->db->query("SELECT cl.id FROM io_business_evaluation cl INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                        $io_business_evaluation_id = [];

                        if ($io_business_evaluation_row->num_rows > 0) {
                            while ($row = $io_business_evaluation_row->fetch_assoc()) {
                                $io_business_evaluation_id[] = $row['id'];
                                echo 'Depurado tabla: io_business_evaluation' . '\n';
                                $this->createInsert($row, 'io_business_evaluation');
                            }
                        }

                        if (empty($io_business_evaluation_id) == false) {
                            //Depuración de io_be_future_business relacionados con OI
                            $io_be_future_business_row = $this->db->query("SELECT fb.id FROM io_be_future_business fb INNER JOIN io_business_evaluation cl ON cl.id = fb.io_be_id INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_be_future_business_row->num_rows > 0) {
                                while ($row = $io_be_future_business_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_be_future_business' . '\n';
                                    $this->createInsert($row, 'io_be_future_business');
                                }
                            }

                            $io_business_evaluation_attachments_row = $this->db->query("SELECT fb.id FROM io_business_evaluation_attachments fb INNER JOIN io_business_evaluation cl ON cl.id = fb.io_business_evaluation_id INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_business_evaluation_attachments_row->num_rows > 0) {
                                while ($row = $io_business_evaluation_attachments_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_business_evaluation_attachments' . '\n';
                                    $this->createInsert($row, 'io_business_evaluation_attachments');
                                }
                            }
                        }

                        //Depuración de io_closing_requests relacionados con OI
                        $io_closing_requests_row = $this->db->query("SELECT cl.id FROM io_closing_requests cl INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                        $io_closing_requests_id = [];

                        if ($io_closing_requests_row->num_rows > 0) {
                            while ($row = $io_closing_requests_row->fetch_assoc()) {
                                $io_closing_requests_id[] = $row['id'];
                                echo 'Depurado tabla: io_closing_requests' . '\n';
                                $this->createInsert($row, 'io_closing_requests');
                            }
                        }

                        //Depuración de io_closing_request_attachments relacionados con OI
                        $io_closing_request_attachments_row = $this->db->query("SELECT fb.id FROM io_closing_request_attachments fb INNER JOIN io_closing_requests cl ON cl.id = fb.io_closing_request_id INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_closing_request_attachments_row->num_rows > 0) {
                            while ($row = $io_closing_request_attachments_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_closing_request_attachments' . '\n';
                                $this->createInsert($row, 'io_closing_request_attachments');
                            }
                        }

                        //Depuración de io_commercial relacionados con OI
                        $io_commercial_row = $this->db->query("SELECT cl.id FROM io_commercial cl INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_commercial_row->num_rows > 0) {
                            while ($row = $io_commercial_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_commercial' . '\n';
                                $this->createInsert($row, 'io_commercial');
                            }
                        }

                        //Depuración de io_costs relacionados con OI
                        $io_costs_row = $this->db->query("SELECT cl.id FROM io_costs cl INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                        $io_costs_id = [];

                        if ($io_costs_row->num_rows > 0) {
                            while ($row = $io_costs_row->fetch_assoc()) {
                                $io_costs_id[] = $row['id'];
                                echo 'Depurado tabla: io_costs' . '\n';
                                $this->createInsert($row, 'io_costs');
                            }
                        }

                        if (empty($io_costs_id) == false) {
                            //Depuración de io_expenses relacionados con OI
                            $io_expenses_row = $this->db->query("SELECT ex.id FROM io_expenses ex INNER JOIN io_costs cl ON cl.id = ex.io_costs_id INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id	 INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_expenses_row->num_rows > 0) {
                                while ($row = $io_expenses_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_expenses' . '\n';
                                    $this->createInsert($row, 'io_expenses');
                                }
                            }

                            $io_manpower_contractor_row = $this->db->query("SELECT ex.id FROM io_manpower_contractor ex INNER JOIN io_costs cl ON cl.id = ex.io_costs_id INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id	 INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_manpower_contractor_row->num_rows > 0) {
                                while ($row = $io_manpower_contractor_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_manpower_contractor' . '\n';
                                    $this->createInsert($row, 'io_manpower_contractor');
                                }
                            }

                            //Depuración de io_manpower_linde relacionados con OI
                            $io_manpower_linde_row = $this->db->query("SELECT ex.id FROM io_manpower_linde ex INNER JOIN io_costs cl ON cl.id = ex.io_costs_id INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id	 INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_manpower_linde_row->num_rows > 0) {
                                while ($row = $io_manpower_linde_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_manpower_linde' . '\n';
                                    $this->createInsert($row, 'io_manpower_linde');
                                }
                            }

                            //Depuración de io_materials_costs relacionados con OI
                            $io_materials_costs_row = $this->db->query("SELECT ex.id FROM io_materials_costs ex INNER JOIN io_costs cl ON cl.id = ex.io_costs_id INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id	 INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_materials_costs_row->num_rows > 0) {
                                while ($row = $io_materials_costs_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_materials_costs' . '\n';
                                    $this->createInsert($row, 'io_materials_costs');
                                }
                            }

                            //Depuración de io_total_budget relacionados con OI
                            $io_total_budget_row = $this->db->query("SELECT ex.id FROM io_total_budget ex INNER JOIN io_costs cl ON cl.id = ex.io_costs_id INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id	 INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($io_total_budget_row->num_rows > 0) {
                                while ($row = $io_total_budget_row->fetch_assoc()) {
                                    echo 'Depurado tabla: io_total_budget' . '\n';
                                    $this->createInsert($row, 'io_total_budget');
                                }
                            }
                        }

                        //Depuración de io_costs relacionados con OI
                        $io_document_budget_row = $this->db->query("SELECT cl.id FROM io_document_budget cl INNER JOIN io_installation_orders c ON c.id = cl.io_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_document_budget_row->num_rows > 0) {
                            while ($row = $io_document_budget_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_document_budget' . '\n';
                                $this->createInsert($row, 'io_document_budget');
                            }
                        }

                        $io_installation_row = $this->db->query("SELECT cl.id FROM io_installation cl INNER JOIN io_installation_orders c ON c.id = cl.io_installation_orders_id INNER JOIN allus_clientes cm ON cm.id = c.idclient INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($io_installation_row->num_rows > 0) {
                            while ($row = $io_installation_row->fetch_assoc()) {
                                echo 'Depurado tabla: io_installation' . '\n';
                                $this->createInsert($row, 'io_installation');
                            }
                        }
                    }

                    //Depuración de oi_config_branchoffice relacionados con Clientes
                    $oi_config_branchoffice_row = $this->db->query("SELECT c.id FROM oi_config_branchoffice c INNER JOIN allus_clientes cm ON cm.id = c.client_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                    if ($oi_config_branchoffice_row->num_rows > 0) {
                        while ($row = $oi_config_branchoffice_row->fetch_assoc()) {
                            echo 'Depurado tabla: oi_config_branchoffice' . '\n';
                            $this->createInsert($row, 'oi_config_branchoffice');
                        }
                    }

                    //Depuración de tc_technical_service relacionados con Clientes
                    $tc_technical_service_row = $this->db->query("SELECT c.id FROM tc_technical_service c INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $tc_technical_service_id = [];

                    if ($tc_technical_service_row->num_rows > 0) {
                        while ($row = $tc_technical_service_row->fetch_assoc()) {
                            $tc_technical_service_id[] = $row['id'];
                            echo 'Depurado tabla: tc_technical_service' . '\n';
                            $this->createInsert($row, 'tc_technical_service');
                        }
                    }

                    if (empty($tc_technical_service_id) == false) {
                        $tc_attachments_row = $this->db->query("SELECT tc.id FROM tc_attachments tc INNER JOIN tc_technical_service c ON c.id = tc.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($tc_attachments_row->num_rows > 0) {
                            while ($row = $tc_attachments_row->fetch_assoc()) {
                                echo 'Depurado tabla: tc_attachments' . '\n';
                                $this->createInsert($row, 'tc_attachments');
                            }
                        }

                        $tc_budget_execution_row = $this->db->query("SELECT tc.id FROM tc_budget_execution tc INNER JOIN tc_technical_service c ON c.id = tc.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($tc_budget_execution_row->num_rows > 0) {
                            while ($row = $tc_budget_execution_row->fetch_assoc()) {
                                echo 'Depurado tabla: tc_budget_execution' . '\n';
                                $this->createInsert($row, 'tc_budget_execution');
                            }
                        }

                        $tc_cases_state_managements_aud_row = $this->db->query("SELECT tc.id FROM tc_cases_state_managements_aud tc INNER JOIN tc_technical_service c ON c.id = tc.id_case INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($tc_cases_state_managements_aud_row->num_rows > 0) {
                            while ($row = $tc_cases_state_managements_aud_row->fetch_assoc()) {
                                echo 'Depurado tabla: tc_cases_state_managements_aud' . '\n';
                                $this->createInsert($row, 'tc_cases_state_managements_aud');
                            }
                        }

                        $tc_mail_notification_row = $this->db->query("SELECT tc.id FROM tc_mail_notification tc INNER JOIN tc_technical_service c ON c.id = tc.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($tc_mail_notification_row->num_rows > 0) {
                            while ($row = $tc_mail_notification_row->fetch_assoc()) {
                                echo 'Depurado tabla: tc_mail_notification' . '\n';
                                $this->createInsert($row, 'tc_mail_notification');
                            }
                        }

                        $tc_service_execution_row = $this->db->query("SELECT tc.id FROM tc_service_execution tc INNER JOIN tc_technical_service c ON c.id = tc.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                        $tc_service_execution_id = [];

                        if ($tc_service_execution_row->num_rows > 0) {
                            while ($row = $tc_service_execution_row->fetch_assoc()) {
                                $tc_service_execution_id[] = $row['id'];
                                echo 'Depurado tabla: tc_service_execution' . '\n';
                                $this->createInsert($row, 'tc_service_execution');
                            }
                        }

                        if (empty($tc_service_execution_id) == false) {
                            $tc_log_execution_st_row = $this->db->query("SELECT ex.id FROM tc_log_execution_st ex INNER JOIN tc_service_execution cl ON cl.id = ex.id_execution_service INNER JOIN tc_technical_service c ON c.id = cl.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($tc_log_execution_st_row->num_rows > 0) {
                                while ($row = $tc_log_execution_st_row->fetch_assoc()) {
                                    echo 'Depurado tabla: tc_log_execution_st' . '\n';
                                    $this->createInsert($row, 'tc_log_execution_st');
                                }
                            }

                            $tc_pending_row = $this->db->query("SELECT ex.id FROM tc_pending ex INNER JOIN tc_service_execution cl ON cl.id = ex.id_service_execution INNER JOIN tc_technical_service c ON c.id = cl.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($tc_pending_row->num_rows > 0) {
                                while ($row = $tc_pending_row->fetch_assoc()) {
                                    echo 'Depurado tabla: tc_pending' . '\n';
                                    $this->createInsert($row, 'tc_pending');
                                }
                            }
                        }

                        $tc_technical_invoice_row = $this->db->query("SELECT tc.id FROM tc_technical_invoice tc INNER JOIN tc_technical_service c ON c.id = tc.id_technical_service INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($tc_technical_invoice_row->num_rows > 0) {
                            while ($row = $tc_technical_invoice_row->fetch_assoc()) {
                                echo 'Depurado tabla: tc_technical_invoice' . '\n';
                                $this->createInsert($row, 'tc_technical_invoice');
                            }
                        }

                        $ts_costs_row = $this->db->query("SELECT tc.id FROM ts_costs tc INNER JOIN tc_technical_service c ON c.id = tc.id_technical_services INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($ts_costs_row->num_rows > 0) {
                            while ($row = $ts_costs_row->fetch_assoc()) {
                                echo 'Depurado tabla: ts_costs' . '\n';
                                $this->createInsert($row, 'ts_costs');
                            }
                        }
                    }

                    //Depuración de allus_casos relacionados con Clientes
                    $allus_casos_row = $this->db->query("SELECT c.id FROM allus_casos c INNER JOIN allus_clientes cm ON cm.id = c.caso_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                    $allus_casos_id = [];

                    if ($allus_casos_row->num_rows > 0) {
                        while ($row = $allus_casos_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_casos' . '\n';
                            $allus_casos_id = $row['id'];
                            $this->createInsert($row, 'allus_casos');
                        }
                    }

                    if (empty($allus_casos_id) == false) {
                        $allus_casos_attachments_row = $this->db->query("SELECT tc.id FROM allus_casos_attachments tc INNER JOIN allus_casos c ON c.id = tc.caso_id INNER JOIN allus_clientes cm ON cm.id = c.caso_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_casos_attachments_row->num_rows > 0) {
                            while ($row = $allus_casos_attachments_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_casos_attachments' . '\n';
                                $this->createInsert($row, 'allus_casos_attachments');
                            }
                        }

                        $allus_is_cotizaciones_row = $this->db->query("SELECT tc.id FROM allus_is_cotizaciones tc INNER JOIN allus_casos c ON c.id = tc.coti_caso_id INNER JOIN allus_clientes cm ON cm.id = c.caso_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_is_cotizaciones_row->num_rows > 0) {
                            while ($row = $allus_is_cotizaciones_row->fetch_assoc()) {
                                $allus_is_cotizaciones_id = $row['id'];
                                echo 'Depurado tabla: allus_is_cotizaciones' . '\n';
                                $this->createInsert($row, 'allus_is_cotizaciones');
                            }
                        }

                        if (empty($allus_is_cotizaciones_id) == false) {
                            $allus_is_cotizaciones_gestiones_row = $this->db->query("SELECT ex.id FROM allus_is_cotizaciones_gestiones ex INNER JOIN allus_is_cotizaciones cl ON cl.id = ex.coge_coti_id INNER JOIN allus_casos c ON c.id = cl.caso_clie_id INNER JOIN allus_clientes cm ON cm.id = c.id_cliente INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($allus_is_cotizaciones_gestiones_row->num_rows > 0) {
                                while ($row = $allus_is_cotizaciones_gestiones_row->fetch_assoc()) {
                                    echo 'Depurado tabla: allus_is_cotizaciones_gestiones' . '\n';
                                    $this->createInsert($row, 'allus_is_cotizaciones_gestiones');
                                }
                            }
                        }

                        $allus_le_actividades_row = $this->db->query("SELECT tc.id FROM allus_le_actividades tc INNER JOIN allus_casos c ON c.id = tc.acti_caso_id INNER JOIN allus_clientes cm ON cm.id = c.caso_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);
                        $allus_le_actividades_id = [];

                        if ($allus_le_actividades_row->num_rows > 0) {
                            while ($row = $allus_le_actividades_row->fetch_assoc()) {
                                $allus_le_actividades_id[] = $row['id'];
                                echo 'Depurado tabla: allus_le_actividades' . '\n';
                                $this->createInsert($row, 'allus_le_actividades');
                            }
                        }

                        if (empty($allus_le_actividades_id) == false) {
                            $allus_le_gestiones_row = $this->db->query("SELECT ex.id FROM allus_le_gestiones ex INNER JOIN allus_le_actividades cl ON cl.id = ex.gest_acti_id INNER JOIN allus_casos c ON c.id = cl.acti_caso_id INNER JOIN allus_clientes cm ON cm.id = c.caso_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                            if ($allus_le_gestiones_row->num_rows > 0) {
                                while ($row = $allus_le_gestiones_row->fetch_assoc()) {
                                    echo 'Depurado tabla: allus_le_gestiones' . '\n';
                                    $this->createInsert($row, 'allus_le_gestiones');
                                }
                            }
                        }

                        $allus_le_actividades_asesor_row = $this->db->query("SELECT tc.id FROM allus_le_actividades_asesor tc INNER JOIN allus_casos c ON c.id = tc.acas_caso_id INNER JOIN allus_clientes cm ON cm.id = c.caso_clie_id INNER JOIN allus_le_paises p ON p.id = cm.clie_pais_id WHERE " . $like_full);

                        if ($allus_le_actividades_asesor_row->num_rows > 0) {
                            while ($row = $allus_le_actividades_asesor_row->fetch_assoc()) {
                                echo 'Depurado tabla: allus_le_actividades_asesor' . '\n';
                                $this->createInsert($row, 'allus_le_actividades_asesor');
                            }
                        }
                    }
                }

                //Depuración de allus_em_templates relacionados con Países
                $allus_em_templates_row = $this->db->query("SELECT c.id FROM allus_em_templates c INNER JOIN allus_le_paises p ON p.id = c.pais_id WHERE " . $like_full);
                $allus_em_templates_id = [];

                if ($allus_em_templates_row->num_rows > 0) {
                    while ($row = $allus_em_templates_row->fetch_assoc()) {
                        $allus_em_templates_id[] = $row['id'];
                        echo 'Depurado tabla: allus_em_templates' . '\n';
                        $this->createInsert($row, 'allus_em_templates');
                    }
                }

                if (empty($allus_em_templates_id) == false) {
                    //Depuración de allus_escalamientos relacionados con Los templates
                    $allus_escalamientos_row = $this->db->query("SELECT c.id FROM allus_escalamientos c INNER JOIN allus_em_templates cm ON cm.id = c.esca_emailtemplate_id INNER JOIN allus_le_paises p ON p.id = cm.pais_id WHERE " . $like_full);
                    $allus_escalamientos_id = [];

                    if ($allus_escalamientos_row->num_rows > 0) {
                        while ($row = $allus_escalamientos_row->fetch_assoc()) {
                            $allus_escalamientos_id[] = $row['id'];
                            echo 'Depurado tabla: allus_escalamientos' . '\n';
                            $this->createInsert($row, 'allus_escalamientos');
                        }
                    }

                    //Depuración de allus_escalamientos relacionados con Los templates
                    $allus_le_paises_emailtemplates_row = $this->db->query("SELECT c.id FROM allus_le_paises_emailtemplates c INNER JOIN allus_em_templates cm ON cm.id = c.paem_emtp_id INNER JOIN allus_le_paises p ON p.id = cm.pais_id WHERE " . $like_full);

                    if ($allus_le_paises_emailtemplates_row->num_rows > 0) {
                        while ($row = $allus_le_paises_emailtemplates_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_le_paises_emailtemplates' . '\n';
                            $this->createInsert($row, 'allus_le_paises_emailtemplates');
                        }
                    }
                }

                //Depuración de allus_grupos relacionados con Países
                $allus_grupos_row = $this->db->query("SELECT c.id FROM allus_grupos c INNER JOIN allus_le_paises p ON p.id = c.grou_pais_id WHERE " . $like_full);
                $allus_grupos_id[] = [];

                if ($allus_grupos_row->num_rows > 0) {
                    while ($row = $allus_grupos_row->fetch_assoc()) {
                        $allus_grupos_id[] = $row['id'];
                        echo 'Depurado tabla: allus_grupos' . '\n';
                        $this->createInsert($row, 'allus_grupos');
                    }
                }

                if (empty($allus_grupos_id) == false) {
                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $allus_grupos_lider_rel_row = $this->db->query("SELECT c.id FROM allus_grupos_lider_rel c INNER JOIN allus_grupos cm ON cm.id = c.grupo_id INNER JOIN allus_le_paises p ON p.id = cm.grou_pais_id WHERE " . $like_full);

                    if ($allus_grupos_lider_rel_row->num_rows > 0) {
                        while ($row = $allus_grupos_lider_rel_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_grupos_lider_rel' . '\n';
                            $this->createInsert($row, 'allus_grupos_lider_rel');
                        }
                    }

                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $allus_rm_level_activites_row = $this->db->query("SELECT c.id FROM allus_rm_level_activites c INNER JOIN allus_grupos cm ON cm.id = c.group_id INNER JOIN allus_le_paises p ON p.id = cm.grou_pais_id WHERE " . $like_full);

                    if ($allus_rm_level_activites_row->num_rows > 0) {
                        while ($row = $allus_rm_level_activites_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_rm_level_activites' . '\n';
                            $this->createInsert($row, 'allus_rm_level_activites');
                        }
                    }

                    //Depuración de allus_em_emails_alerts relacionados con Grupos
                    $allus_em_emails_alerts_row = $this->db->query("SELECT c.id FROM allus_em_emails_alerts c INNER JOIN allus_grupos cm ON cm.id = c.grupo_id INNER JOIN allus_le_paises p ON p.id = cm.grou_pais_id WHERE " . $like_full);

                    if ($allus_em_emails_alerts_row->num_rows > 0) {
                        while ($row = $allus_em_emails_alerts_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_em_emails_alerts' . '\n';
                            $this->createInsert($row, 'allus_em_emails_alerts');
                        }
                    }

                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $users_allus_grupos_row = $this->db->query("SELECT c.id FROM users_allus_grupos c INNER JOIN allus_grupos cm ON cm.id = c.allus_grupo_id INNER JOIN allus_le_paises p ON p.id = cm.grou_pais_id WHERE " . $like_full);

                    if ($users_allus_grupos_row->num_rows > 0) {
                        while ($row = $users_allus_grupos_row->fetch_assoc()) {
                            echo 'Depurado tabla: users_allus_grupos' . '\n';
                            $this->createInsert($row, 'users_allus_grupos');
                        }
                    }

                    //Depuración de allus_grupos_lider_rel relacionados con Grupos
                    $allus_em_areas_escalamiento_email_row = $this->db->query("SELECT c.id FROM allus_em_areas_escalamiento_email c INNER JOIN allus_grupos cm ON cm.id = c.em_email_id INNER JOIN allus_le_paises p ON p.id = cm.grou_pais_id WHERE " . $like_full);

                    if ($allus_em_areas_escalamiento_email_row->num_rows > 0) {
                        while ($row = $allus_em_areas_escalamiento_email_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_em_areas_escalamiento_email' . '\n';
                            $this->createInsert($row, 'allus_em_areas_escalamiento_email');
                        }
                    }
                }

                //Depuración de allus_holidays relacionados con Países
                $allus_holidays_row = $this->db->query("SELECT c.id FROM allus_holidays c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($allus_holidays_row->num_rows > 0) {
                    while ($row = $allus_holidays_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_holidays' . '\n';
                        $this->createInsert($row, 'allus_holidays');
                    }
                }

                //Depuración de allus_le_arbol_actividades relacionados con Países
                $allus_le_arbol_actividades_row = $this->db->query("SELECT c.id FROM allus_le_arbol_actividades c INNER JOIN allus_le_paises p ON p.id = c.arac_pais_id WHERE " . $like_full);

                if ($allus_le_arbol_actividades_row->num_rows > 0) {
                    while ($row = $allus_le_arbol_actividades_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_arbol_actividades' . '\n';
                        $this->createInsert($row, 'allus_le_arbol_actividades');
                    }
                }

                if (empty($allus_le_arbol_actividades_id) == false) {
                    //Depuración de allus_le_arbol_actividades_escalamientos relacionados con Países
                    $allus_le_arbol_actividades_escalamientos_row = $this->db->query("SELECT c.id FROM allus_le_arbol_actividades_escalamientos c INNER JOIN allus_le_arbol_actividades cm ON cm.id = c.araces_arac_id INNER JOIN allus_le_paises p ON p.id = cm.arac_pais_id WHERE " . $like_full);

                    if ($allus_le_arbol_actividades_escalamientos_row->num_rows > 0) {
                        while ($row = $allus_le_arbol_actividades_escalamientos_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_le_arbol_actividades_escalamientos' . '\n';
                            $this->createInsert($row, 'allus_le_arbol_actividades_escalamientos');
                        }
                    }

                    //Depuración de allus_le_arbol_actividades_escalamientos relacionados con Países
                    $allus_le_actividades_row = $this->db->query("SELECT c.id FROM allus_le_actividades c INNER JOIN allus_le_arbol_actividades cm ON cm.id = c.acti_baseactvi_id INNER JOIN allus_le_paises p ON p.id = cm.arac_pais_id WHERE " . $like_full);

                    if ($allus_le_actividades_row->num_rows > 0) {
                        while ($row = $allus_le_actividades_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_le_actividades' . '\n';
                            $this->createInsert($row, 'allus_le_actividades');
                        }
                    }

                    //Depuración de allus_le_actividades_asesor relacionados con Países
                    $allus_le_actividades_asesor_row = $this->db->query("SELECT c.id FROM allus_le_actividades_asesor c INNER JOIN allus_le_arbol_actividades cm ON cm.id = c.acas_baseactvi_id INNER JOIN allus_le_paises p ON p.id = cm.arac_pais_id WHERE " . $like_full);

                    if ($allus_le_actividades_asesor_row->num_rows > 0) {
                        while ($row = $allus_le_actividades_asesor_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_le_actividades_asesor' . '\n';
                            $this->createInsert($row, 'allus_le_actividades_asesor');
                        }
                    }
                }

                //Depuración de branch_offices relacionados con Países
                $branch_offices_row = $this->db->query("SELECT c.id FROM branch_offices c INNER JOIN allus_le_paises p ON p.id = c.id_country WHERE " . $like_full);
                $branch_offices_row_id = [];

                if ($branch_offices_row->num_rows > 0) {
                    while ($row = $branch_offices_row->fetch_assoc()) {
                        echo 'Depurado tabla: branch_offices' . '\n';
                        $branch_offices_row_id[] = $row['id'];
                        $this->createInsert($row, 'branch_offices');
                    }
                }

                if (empty($branch_offices_row_id) == false) {
                    //Depuración de doa_flow_levels_ranges relacionados con Países
                    $doa_flow_levels_ranges_row = $this->db->query("SELECT c.id FROM doa_flow_levels_ranges c INNER JOIN branch_offices cm ON cm.id = c.branch_office INNER JOIN allus_le_paises p ON p.id = cm.id_country WHERE " . $like_full);

                    if ($doa_flow_levels_ranges_row->num_rows > 0) {
                        while ($row = $doa_flow_levels_ranges_row->fetch_assoc()) {
                            echo 'Depurado tabla: doa_flow_levels_ranges' . '\n';
                            $this->createInsert($row, 'doa_flow_levels_ranges');
                        }
                    }
                }

                //Depuración de currencies relacionados con Países
                $currencies_row = $this->db->query("SELECT c.id FROM currencies c INNER JOIN allus_le_paises p ON p.id = c.allus_le_pais_id WHERE " . $like_full);
                $currencies_id = [];

                if ($currencies_row->num_rows > 0) {
                    while ($row = $currencies_row->fetch_assoc()) {
                        $currencies_id[] = $row['id'];
                        echo 'Depurado tabla: currencies' . '\n';
                        $this->createInsert($row, 'currencies');
                    }
                    $this->deleteLogGeneral($currencies_id, 'currencies', 'currency_id');
                }

                if (empty($currencies_id) == false) {
                    //Depuración de currencies_logs relacionados con Países
                    $currencies_logs_row = $this->db->query("SELECT c.id FROM currencies_logs c INNER JOIN currencies cm ON cm.id = c.currency_id INNER JOIN allus_le_paises p ON p.id = cm.allus_le_pais_id WHERE " . $like_full);

                    if ($currencies_logs_row->num_rows > 0) {
                        while ($row = $currencies_logs_row->fetch_assoc()) {
                            echo 'Depurado tabla: currencies_logs' . '\n';
                            $this->createInsert($row, 'currencies_logs');
                        }
                    }

                    //Depuración de campaigns relacionados con Países
                    $campaigns_row = $this->db->query("SELECT c.id FROM campaigns c INNER JOIN currencies cm ON cm.id = c.currency_id INNER JOIN allus_le_paises p ON p.id = cm.allus_le_pais_id WHERE " . $like_full);
                    if ($campaigns_row->num_rows > 0) {
                        while ($row = $campaigns_row->fetch_assoc()) {
                            echo 'Depurado tabla: campaigns' . '\n';
                            $this->createInsert($row, 'campaigns');
                        }
                    }
                }

                //Depuración de allus_le_archivos_to_allus_le_paises_rel relacionados con Países
                $allus_le_archivos_to_allus_le_paises_rel_row = $this->db->query("SELECT c.id FROM allus_le_archivos_to_allus_le_paises_rel c INNER JOIN allus_le_paises p ON p.id = c.allus_le_paises_id WHERE " . $like_full);

                if ($allus_le_archivos_to_allus_le_paises_rel_row->num_rows > 0) {
                    while ($row = $allus_le_archivos_to_allus_le_paises_rel_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_archivos_to_allus_le_paises_rel' . '\n';
                        $this->createInsert($row, 'allus_le_archivos_to_allus_le_paises_rel');
                    }
                }

                //Depuración de countries_roles asociadas a las cuentas eliminadas
                $countries_roles_row = $this->db->query("SELECT c.id FROM countries_roles c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($countries_roles_row->num_rows > 0) {
                    while ($row = $countries_roles_row->fetch_assoc()) {
                        echo 'Depurado tabla: countries_roles' . '\n';
                        $this->createInsert($row, 'countries_roles');
                    }
                }

                //Depuración de countries_roles asociadas a las cuentas eliminadas
                $allus_le_festivos_row = $this->db->query("SELECT c.id FROM allus_le_festivos c INNER JOIN allus_le_paises p ON p.id = c.allus_le_pais_id WHERE " . $like_full);

                if ($allus_le_festivos_row->num_rows > 0) {
                    while ($row = $allus_le_festivos_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_festivos' . '\n';
                        $this->createInsert($row, 'allus_le_festivos');
                    }
                }

                //Depuración de allus_le_genesis_email asociadas a las cuentas eliminadas
                $allus_le_genesis_email_row = $this->db->query("SELECT c.id FROM allus_le_genesis_email c INNER JOIN allus_le_paises p ON p.id = c.geem_pais_name WHERE " . $like_full);

                if ($allus_le_genesis_email_row->num_rows > 0) {
                    while ($row = $allus_le_genesis_email_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_genesis_email' . '\n';
                        $allus_le_genesis_email_id = $row['id'];
                        $this->createInsert($row, 'allus_le_genesis_email');
                    }
                }

                if (empty($allus_le_genesis_email_id) == false) {
                    //Depuración de allus_le_genesis_email_adjuntos asociadas a las cuentas eliminadas
                    $allus_le_genesis_email_adjuntos_row = $this->db->query("SELECT c.id FROM allus_le_genesis_email_adjuntos c INNER JOIN allus_le_genesis_email cm ON cm.id = c.geea_gene_email_name INNER JOIN allus_le_paises p ON p.id = cm.geem_pais_name WHERE " . $like_full);

                    if ($allus_le_genesis_email_adjuntos_row->num_rows > 0) {
                        while ($row = $allus_le_genesis_email_adjuntos_row->fetch_assoc()) {
                            echo 'Depurado tabla: allus_le_genesis_email' . '\n';
                            $this->createInsert($row, 'allus_le_genesis_email_adjuntos');
                        }
                    }
                }

                //Depuración de allus_le_horarios_asignacion asociadas a las cuentas eliminadas
                $allus_le_horarios_asignacion_row = $this->db->query("SELECT c.id FROM allus_le_horarios_asignacion c INNER JOIN allus_le_paises p ON p.id = c.hoas_pais_id WHERE " . $like_full);

                if ($allus_le_horarios_asignacion_row->num_rows > 0) {
                    while ($row = $allus_le_horarios_asignacion_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_horarios_asignacion' . '\n';
                        $this->createInsert($row, 'allus_le_horarios_asignacion');
                    }
                }

                //Depuración de allus_le_noticias_to_allus_le_paises_rel asociadas a los Países
                $allus_le_noticias_to_allus_le_paises_rel_row = $this->db->query("SELECT c.id FROM allus_le_noticias_to_allus_le_paises_rel c INNER JOIN allus_le_paises p ON p.id = c.allus_le_paises WHERE " . $like_full);

                if ($allus_le_noticias_to_allus_le_paises_rel_row->num_rows > 0) {
                    while ($row = $allus_le_noticias_to_allus_le_paises_rel_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_horarios_asignacion' . '\n';
                        $this->createInsert($row, 'allus_le_noticias_to_allus_le_paises_rel');
                    }
                }

                //Depuración de allus_le_paises_emailaccounts asociadas a los Países
                $allus_le_paises_emailaccounts_row = $this->db->query("SELECT c.id FROM allus_le_paises_emailaccounts c INNER JOIN allus_le_paises p ON p.id = c.pacc_pais_id WHERE " . $like_full);

                if ($allus_le_paises_emailaccounts_row->num_rows > 0) {
                    while ($row = $allus_le_paises_emailaccounts_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_paises_emailaccounts' . '\n';
                        $this->createInsert($row, 'allus_le_paises_emailaccounts');
                    }
                }

                //Depuración de allus_le_paises_roles asociadas a los Países
                $allus_le_paises_roles_row = $this->db->query("SELECT c.id FROM allus_le_paises_roles c INNER JOIN allus_le_paises p ON p.id = c.paro_pais_id WHERE " . $like_full);

                if ($allus_le_paises_roles_row->num_rows > 0) {
                    while ($row = $allus_le_paises_roles_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_paises_roles' . '\n';
                        $this->createInsert($row, 'allus_le_paises_roles');
                    }
                }

                //Depuración de allus_le_vendedores asociadas a los Países
                $allus_le_vendedores_row = $this->db->query("SELECT c.id FROM allus_le_vendedores c INNER JOIN allus_le_paises p ON p.id = c.vend_pais_id WHERE " . $like_full);

                if ($allus_le_vendedores_row->num_rows > 0) {
                    while ($row = $allus_le_vendedores_row->fetch_assoc()) {
                        echo 'Depurado tabla: allus_le_vendedores' . '\n';
                        $this->createInsert($row, 'allus_le_vendedores');
                    }
                }

                //Depuración de doa_flow asociadas a los Países
                $doa_flow_row = $this->db->query("SELECT c.id FROM doa_flow c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);
                $doa_flow_id = [];

                if ($doa_flow_row->num_rows > 0) {
                    while ($row = $doa_flow_row->fetch_assoc()) {
                        echo 'Depurado tabla: doa_flow' . '\n';
                        $doa_flow_id[] = $row['id'];
                        $this->createInsert($row, 'doa_flow');
                    }
                }

                if (empty($doa_flow_id) == false) {
                    //Depuración de doa_flow_ranges asociadas a los Países
                    $doa_flow_ranges_row = $this->db->query("SELECT c.id FROM doa_flow_ranges c INNER JOIN doa_flow cm ON cm.id = c.doa_flow_id INNER JOIN allus_le_paises p ON p.id = cm.country_id WHERE " . $like_full);
                    $doa_flow_ranges_id = [];

                    if ($doa_flow_ranges_row->num_rows > 0) {
                        while ($row = $doa_flow_ranges_row->fetch_assoc()) {
                            $doa_flow_ranges_id[] = $row['id'];
                            echo 'Depurado tabla: doa_flow_ranges' . '\n';
                            $this->createInsert($row, 'doa_flow_ranges');
                        }
                    }

                    //Depuración de doa_flow_levels_ranges asociadas a los Países
                    $doa_flow_levels_ranges_row = $this->db->query("SELECT d.id FROM doa_flow_levels_ranges d INNER JOIN doa_flow_ranges c ON c.id = d.doa_flow_range_id INNER JOIN doa_flow cm ON cm.id = c.doa_flow_id INNER JOIN allus_le_paises p ON p.id = cm.country_id WHERE " . $like_full);

                    if ($doa_flow_levels_ranges_row->num_rows > 0) {
                        while ($row = $doa_flow_levels_ranges_row->fetch_assoc()) {
                            echo 'Depurado tabla: doa_flow_levels_ranges' . '\n';
                            $this->createInsert($row, 'doa_flow_levels_ranges');
                        }
                    }
                }

                //Depuración de doa_flow asociadas a los Países
                $genesis_email_row = $this->db->query("SELECT c.id FROM genesis_email c INNER JOIN allus_le_paises p ON p.id = c.geem_pais_name WHERE " . $like_full);

                if ($genesis_email_row->num_rows > 0) {
                    while ($row = $genesis_email_row->fetch_assoc()) {
                        echo 'Depurado tabla: genesis_email' . '\n';
                        $this->createInsert($row, 'genesis_email');
                    }
                }

                //Depuración de hiq_specifications asociadas a los Países
                $hiq_specifications_row = $this->db->query("SELECT c.id FROM hiq_specifications c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($hiq_specifications_row->num_rows > 0) {
                    while ($row = $hiq_specifications_row->fetch_assoc()) {
                        echo 'Depurado tabla: hiq_specifications' . '\n';
                        $this->createInsert($row, 'hiq_specifications');
                    }
                }

                //Depuración de hiq_cylinders asociadas a los Países
                $hiq_cylinders_row = $this->db->query("SELECT c.id FROM hiq_specifications c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($hiq_cylinders_row->num_rows > 0) {
                    while ($row = $hiq_cylinders_row->fetch_assoc()) {
                        echo 'Depurado tabla: hiq_cylinders' . '\n';
                        $this->createInsert($row, 'hiq_cylinders');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $oi_config_amounts_row = $this->db->query("SELECT c.id FROM oi_config_amounts c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($oi_config_amounts_row->num_rows > 0) {
                    while ($row = $oi_config_amounts_row->fetch_assoc()) {
                        echo 'Depurado tabla: oi_config_amounts' . '\n';
                        $this->createInsert($row, 'oi_config_amounts');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $oi_config_branchoffice_row = $this->db->query("SELECT c.id FROM oi_config_branchoffice c INNER JOIN allus_le_paises p ON p.id = c.country_id WHERE " . $like_full);

                if ($oi_config_branchoffice_row->num_rows > 0) {
                    while ($row = $oi_config_branchoffice_row->fetch_assoc()) {
                        echo 'Depurado tabla: oi_config_amounts' . '\n';
                        $this->createInsert($row, 'oi_config_amounts');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $tc_dropdowns_country_row = $this->db->query("SELECT c.id FROM tc_dropdowns_country c INNER JOIN allus_le_paises p ON p.id = c.id_country WHERE " . $like_full);

                if ($tc_dropdowns_country_row->num_rows > 0) {
                    while ($row = $tc_dropdowns_country_row->fetch_assoc()) {
                        echo 'Depurado tabla: tc_dropdowns_country' . '\n';
                        $this->createInsert($row, 'tc_dropdowns_country');
                    }
                }

                //Depuración de oi_config_amounts asociadas a los Países
                $users_allus_le_paises_row = $this->db->query("SELECT c.id FROM users_allus_le_paises c INNER JOIN allus_le_paises p ON p.id = c.allus_le_paises_id WHERE " . $like_full);

                if ($users_allus_le_paises_row->num_rows > 0) {
                    while ($row = $users_allus_le_paises_row->fetch_assoc()) {
                        echo 'Depurado tabla: users_allus_le_paises' . '\n';
                        $this->createInsert($row, 'users_allus_le_paises');
                    }
                }

                //Depuración de allus_le_paises
                foreach ($countries as $key => $object) {
                    $this->createInsert($object, 'allus_le_paises');
                }
            }

            //$this->copyTableFull();
            $this->db->close();
            echo 'Fin de la depuración';
        } else {
            echo 'El o los Países seleccionado en el criterio de Depuración no tiene datos almacenados.';
        }
    }

    /**
     * @param $object
     * @param $table
     * @etrurn bool
     * Función para eliminar registros de una tabla y escribir el archivo con el Insert y toda la información
     */
    public function deleteRow($id, $table)
    {
        if (empty($id) == false && empty($table) == false) {
            $this->db->query('DELETE FROM ' . $table . ' WHERE id LIKE \'' . $id . '\'');
        }
        return true;
    }

    /**
     * @param $object
     * @param $table
     * @return bool
     * Función para eliminar registros de una tabla y escribir el archivo con el Insert y toda la información
     */
    public function createInsertUsers($object, $table)
    {
        if (empty($object) == false && empty($table) == false) {
            /*$insert = 'INSERT IGNORE INTO ' . $table;
            $names = '';
            $values = '';

            foreach ($object as $key => $value) {
                if (empty($names) == false) {
                    $names .= ',';
                }
                $names .= $key;

                if (empty($values) == false) {
                    $values .= ',';
                }
                $values .= '\'' . $value . '\'';
            }

            if (empty($names) == false && empty($values) == false) {
                $insert .= '(' . $names . ') VALUES (' . $values . ');';
                $file = fopen("insert.txt", "a+");
                fwrite($file, $insert . PHP_EOL);
                fclose($file);
            }*/
        }
        return true;
    }

    /**
     * @param $objects_id
     * @param $table
     * @param $column_parent
     * @return bool
     * Función recursiva para eliminar los registros Padres
     */
    public function deleteParentsGeneral($objects_id, $table, $column_parent)
    {
        if (empty($objects_id) == false && empty($table) == false && empty($column_parent) == false) {
            $objects_id = implode("', '", $objects_id);
            $objects_row = $this->db->query("SELECT * FROM " . $table . " WHERE id IN ('" . $objects_id . "')");
            $objects = [];
            $objects_parents = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    if (empty($object[$column_parent]) == false) {
                        $objects_parents[] = $object[$column_parent];
                    }
                    $this->createInsert($object, $table);
                }

                if (empty($objects_parents) == false) {
                    $this->deleteParentsGeneral($objects_parents, $table, $column_parent);
                } else {
                    return false;
                }
            }
        }
    }

    public function deleteLogGeneral($objects_id, $table, $column)
    {
        if (empty($objects_id) == false && empty($table) == false && empty($column) == false) {
            $objects_id = implode("', '", $objects_id);
            $objects_row = $this->db->query("SELECT * FROM " . $table . "_logs WHERE " . $column . " IN ('" . $objects_id . "')");
            $objects = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    $this->createInsert($object, $table);
                }
            }
        }
    }


    public function deleteBugGeneral($objects_id, $table, $column)
    {
        if (empty($objects_id) == false && empty($table) == false && empty($column) == false) {
            $objects_id = implode("', '", $objects_id);
            $objects_row = $this->db->query("SELECT * FROM " . $table . "_bug WHERE " . $column . " IN ('" . $objects_id . "')");
            $objects = [];

            if ($objects_row->num_rows > 0) {
                while ($row = $objects_row->fetch_assoc()) {
                    $objects[] = $row;
                }

                foreach ($objects as $key => $object) {
                    $this->createInsert($object, $table);
                }
            }
        }
    }

    public function copyTableFull()
    {
        $tables = [
            'accounts',
            'accounts_cases',
            'accounts_contacts',
            'accounts_opportunities',
            'acl_actions',
            'acl_applications',
            'acl_relationship_applications',
            'acl_roles',
            'acl_roles_users',
            'acl_roles_actions',
            'activity_sub_activities',
            'allus_admin',
            'allus_admin_audit',
            'allus_arbol',
            'allus_arbol_audit',
            'allus_arbol_camposadicionales_asociados',
            'allus_arbol_camposadicionales_asociados_audit',
            'allus_arbol_roles',
            'allus_arbol_roles_audit',
            'allus_le_archivos',
            'allus_le_archivos_roles',
            'allus_le_archivos_to_aclroles_rel',
            'allus_archivos_adjuntos',
            'allus_archivos_adjuntos_audit',
            'allus_archivos_roles',
            'allus_archivos_roles_audit',
            'allus_sc_mainui',
            'allus_uh_moviles_to_allus_uh_horarios_rel',
            'allus_uh_mutualistas_to_allus_uh_equipos_rel',
            'allus_horario_laboral',
            'allus_horario_laboral_rangos',
            'allus_le_campanabulk',
            'allus_le_campanabulk_audit',
            'allus_le_campanabulk_registros',
            'allus_le_campanabulk_registros_audit',
            'allus_le_clientesbulk_contactos_xref',
            'allus_le_clientesbulk_conversiones',
            'allus_le_clientesbulk_conversiones_audit',
            'allus_le_clientesbulk_tanques',
            'allus_le_clientesbulk_tanques_audit',
            'allus_le_clientesbulk_emails',
            'allus_le_clientesbulk_emails_audit',
            'allus_le_campanabulk_registros_reportes_niveles',
            'allus_le_campanabulk_registros_reportes_niveles_tanques',
            'allus_le_cargasbulk_calllist',
            'allus_le_cargasbulk_calllist_audit',
            'allus_le_cargasbulk_calllist_registros',
            'allus_le_cargasbulk_calllist_registros_audit',
            'allus_le_cargasbulk_clientes',
            'allus_le_cargasbulk_clientes_audit',
            'allus_le_cargasbulk_clientes_registros',
            'allus_le_cargasbulk_clientes_registros_audit',
            'allus_le_cargasbulk_tanques',
            'allus_le_cargasbulk_tanques_audit',
            'allus_le_cargasbulk_tanques_registros',
            'allus_le_cargasbulk_tanques_registros_audit',
            'allus_le_cargasclientes',
            'allus_le_cargasclientes_registros',
            'allus_le_clientesbulk',
            'allus_le_clientesbulk_audit',
            'allus_le_clientesbulk_contactos',
            'allus_le_clientesbulk_contactos_audit',
            'allus_le_clientesbulk_homologaciones',
            'allus_le_clientesbulk_homologaciones_audit',
            'allus_le_clientesbulk_tanques_audit',
            'allus_le_clientesbulk_telefonos',
            'allus_le_productos',
            'allus_le_productos_audit',
            'allus_library',
            'allus_library_audit',
            'allus_le_noticias',
            'allus_le_noticias_roles',
            'allus_le_noticias_to_aclroles_rel',
            'allus_le_archivos_cerificados',
            'allus_le_archivos_cerificados_audit',
            'allus_em_email_frequent_addresses',
            'allus_relacion_3dl',
            'allus_relacion_3dl_audit',
            'allus_reports',
            'allus_rm_level',
            'allus_ruta_estados',
            'allus_ruta_estados_audit',
            'allus_ruta_estados_detalles',
            'allus_ruta_estados_detalles_audit',
            'allus_sc_assets',
            'allus_sc_assets_audit',
            'allus_sc_availabilities',
            'allus_sc_availabilities_audit',
            'allus_si',
            'allus_si_audit',
            'allus_workflow',
            'allus_workflow_actions',
            'allus_workflow_actions_audit',
            'allus_workflow_audit',
            'allus_workflow_conditions',
            'allus_workflow_conditions_audit',
            'allus_workflow_log',
            'allus_workflow_log_audit',
            'allus_le_paises_audit',
            'allus_clientes_audit',
            'allus_contactos_audit',
            'allus_interacciones_audit',
            'allus_sc_appointments_audit',
            'allus_casos_audit',
            'allus_le_actividades_audit',
            'allus_le_gestiones_audit',
            'allus_escalamientos_audit',
            'allus_le_paises_emailtemplates_audit',
            'allus_le_arbol_actividades_audit',
            'allus_le_arbol_actividades_escalamientos_audit',
            'allus_le_actividades_asesor_audit',
            'campaigns_audit',
            'allus_le_festivos_audit',
            'allus_le_horarios_asignacion_audit',
            'allus_le_paises_emailaccounts_audit',
            'allus_le_paises_roles_audit',
            'allus_le_vendedores_audit',
            'address_book',
            'bugs',
            'bugs_audit',
            'calls',
            'cases',
            'cases_audit',
            'cases_bugs',
            'calls_contacts',
            'calls_leads',
            'call_scheduling',
            'cases_notes',
            'calls_users',
            'campaign_trkrs',
            'campaign_log',
            'causes',
            'config',
            'contacts',
            'contacts_bugs',
            'contacts_audit',
            'contacts_cases',
            'cron_remove_documents',
            'custom_fields',
            'documents',
            'documents_cases',
            'document_revisions',
            'documents_accounts',
            'documents_bugs',
            'documents_contacts',
            'documents_opportunities',
            'eapm',
            'emails',
            'emails_email_addr_rel',
            'emails_beans',
            'email_addresses',
            'emailman',
            'email_marketing',
            'email_marketing_prospect_lists',
            'email_cache',
            'emails_text',
            'email_addr_bean_rel',
            'fields_meta_data',
            'folders',
            'folders_rel',
            'folders_subscriptions',
            'hiq_components',
            'hiq_status',
            'hiq_import',
            'hiq_import_log',
            'igma_logs',
            'igma_logs_audit',
            'import_maps',
            'import_clientes_bulk_contactos',
            'import_clientes_bulk_conversiones',
            'import_clientes_bulk_email_preferred',
            'import_clientes_bulk_emails',
            'import_vendedores',
            'inbound_email',
            'inbound_email_autoreply',
            'leads',
            'leads_audit',
            'lems_states',
            'job_queue',
            'leads_audit',
            'linked_documents',
            'meetings',
            'meetings_contacts',
            'meetings_users',
            'meetings_leads',
            'notes',
            'inbound_email_cache_ts',
            'io_sequential',
            'oauth_nonce',
            'oauth_consumer',
            'oauth_tokens',
            'migrations',
            'oauth_nonce',
            'opportunities',
            'opportunities_audit',
            'opportunities_contacts',
            'outbound_email',
            'permission_role',
            'permissions',
            'password_resets',
            'project',
            'projects_accounts',
            'project_task',
            'project_task_audit',
            'projects_cases',
            'projects_contacts',
            'projects_opportunities',
            'projects_products',
            'prospect_list_campaigns',
            'prospect_lists',
            'prospect_lists_prospects',
            'prospects',
            'releases',
            'relationships',
            'reports_filters',
            'role_applications',
            'reports',
            'role_user',
            'roles',
            'roles_users',
            'roles_modules',
            'sessions',
            'saved_search',
            'schedulers',
            'seeds',
            'sugarfeed',
            'sessions',
            'tito_sign',
            'tasks',
            'tc_answer_option',
            'tc_call_response',
            'tc_dropdown',
            'tc_field_dropdown',
            'tc_inquiry',
            'tc_inquiry_questions',
            'tc_inquiry_response',
            'tc_log_execution_st',
            'tc_olsons',
            'tc_relation_dropdowns',
            'tc_olfather',
            'ts_manpower',
            'ts_materials',
            'tracker',
            'upgrade_history',
            'user_preferences',
            'users_applications',
            'users_feeds',
            'users_last_import',
            'users_signatures',
            'users_password_link',
            'versions',
            'vcals'
        ];

        foreach ($tables as $table) {
            if (empty($table) == false) {
                $sql_row = $this->db->query("SELECT * FROM " . $table);
                $sql = [];

                if ($sql_row->num_rows > 0) {
                    while ($row = $sql_row->fetch_assoc()) {
                        $sql[] = $row;
                    }

                    foreach ($sql as $key => $object) {
                        $insert = 'INSERT IGNORE INTO ' . $table;
                        $names = '';
                        $values = '';

                        foreach ($object as $key => $value) {
                            if (empty($names) == false) {
                                $names .= ',';
                            }
                            $names .= $key;

                            if (empty($values) == false) {
                                $values .= ',';
                            }
                            $values .= '\'' . $value . '\'';
                        }

                        if (empty($names) == false && empty($values) == false) {
                            $insert .= '(' . $names . ') VALUES (' . $values . ');';
                            $file = fopen("insert.txt", "a+");
                            fwrite($file, $insert . PHP_EOL);
                            fclose($file);
                        }
                    }
                }
            }
        }
    }
}