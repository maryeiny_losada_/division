<?php
    if(empty($_GET['min']) == true && empty($_GET['max']) == true):
        $_GET['min'] = 0;
        $_GET['max'] = 500;
    endif;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Importación DB - CRM LINDE</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
        form.form-signin {
            max-width: 550px;
            padding: 15px;
            margin: 0 auto;
            top: 50px;
            position: relative;
        }

        .btn-lg{
            margin-top: 20px;
        }

        .mb-10{
            margin-bottom: 10px;
        }

        #loader {
            position: absolute;
            left: 50%;
            top: 50%;
            z-index: 1;
            width: 150px;
            height: 150px;
            margin: -75px 0 0 -75px;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 120px;
            height: 120px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
            display: none;
        }

        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }

        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }

        /* Add animation to "page content" */
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }

        @-webkit-keyframes animatebottom {
            from { bottom:-100px; opacity:0 }
            to { bottom:0px; opacity:1 }
        }

        @keyframes animatebottom {
            from{ bottom:-100px; opacity:0 }
            to{ bottom:0; opacity:1 }
        }

        #myDiv {
            display: none;
            text-align: center;
        }
    </style>
</head>

<body>

<div id="loader"></div>

<div class="container">
    <div id="confirm-importacion">
        <p>¿Está seguro de la Base de Datos ingresada?</p>
    </div>

    <form id="form-importacion" action="route.php" class="form-signin" method="POST" enctype="multipart/form-data">
        <h2 class="form-signin-heading text-center mb-4">Importación de Base de Datos</h2>

        <?php if(empty($_GET['message']) == false): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                Se realizó la Importación correctamente.
            </div>
        <?php endif; ?>

        <?php if(empty($_GET['error']) == false): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php echo $_GET['error']; ?>
            </div>
        <?php endif; ?>

        <label for="namebd" class="sr-only">Nombre de la Base de Datos</label>
        <input type="hidden" id="type" name="type" value="import" required>
        <input type="text" id="namebd" name="namebd" class="form-control mb-10" placeholder="Nombre de la BD" required>
        <input type="file" id="fileinsert" name="fileinsert" class="form-control mb-10" required>

        <button type="submit" class="d-none">Hidden</button>
        <div class="row mt-2">
            <div class="col-md-6">
                <button type="button" id="importar" class="btn btn-primary btn-block">Importar SQL</button>
            </div>

            <div class="col-md-6">
                <button type="button" id="depurar" class="btn btn-primary btn-block">Depurar</button>
            </div>
        </div>
    </form>
</div>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script data-require="bootstrap@*" data-semver="3.1.1" src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#confirm-importacion").dialog({
            autoOpen: false,
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            buttons: {
                "Aceptar": function() {
                    $(this).dialog("close");
                    $('.container').hide();
                    $("#loader").show();
                    $("#form-importacion").submit();
                },
                "Cancelar": function() {
                    $(this).dialog( "close" );
                }
            }
        });

        $('#importar').click(function () {
            $("#confirm-importacion").dialog("open");
        })

        $('#depurar').click(function () {
            window.location.href = "./index.php";
        })
    })
</script>
</body>
</html>
