<?php

echo 'Cargando...';

if(!isset($_SESSION)) {
    session_start();
}

require_once 'controller/index.php';
require_once 'controller/import.php';

if(empty($_GET) == false){
    if(empty($_GET['namebd']) == false && empty($_GET['country']) == false){
        $namebd = $_GET['namebd'];
        $country = $_GET['country'];
        $controller = new IndexController($namebd, $country);
        $controller->depuration();
    }else{
        header("Location: ./index.php");
    }
}else if((empty($_POST) == false && empty($_FILES) == false) || (empty($_SESSION) == false)){
    if((empty($_POST['type']) == false && empty($_FILES['fileinsert']) == false && empty($_POST['namebd']) == false) ||
        (empty($_SESSION['fileName']) == false && empty($_SESSION['nameDB']) == false)){

        if(empty($_GET['init']) == false){
            $init = $_GET['init'];
        }else{
            $init = 0;
        }

        if(empty($_POST) == false){
            $namebd = $_POST['namebd'];
            $fileinsert = $_FILES['fileinsert'];
            $controller = new ImportController($namebd, $fileinsert, $init);
        }else if(empty($_SESSION) == false){
            $namebd = $_SESSION['nameDB'];
            $fileName = $_SESSION['fileName'];
            $init = $_SESSION['init'];
            $controller = new ImportController($namebd, null, $init, $fileName);
        }
        $controller->import();
    }else{
        header("Location: ./import.php");
    }
}else{
    header("Location: ./index.php");
}
